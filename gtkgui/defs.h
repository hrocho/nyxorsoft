/**
 * @file defs.h
 * A.Prochazka
 * last modification 31.5.2016
 * upgrade to nxyter v2
 */ 

#ifndef DEFS_H
#define DEFS_H

#define DEFAULT_SOCKNAME "x86l-12"
#define SHORTCUTS 
#define DEBUG
#define GEMEX_MODE 1


/** global registers structures **/
struct globalreg greg, greg0;
/** nxyter registers structures pointers**/
struct nxyreg *reg=NULL,*reg0=NULL;

/** string to store the name of opened file **/
GString *global_filename; 

/** the main window **/
GtkWidget *global_window;

/** label with SFP configuration **/
GtkWidget *label_sfpconfig; 

/** SFP selector widget **/
GtkWidget *sfp_widget; 
/** EXP selector widget **/
GtkWidget *exp_widget;  

#ifndef GEMEX_MODE
/** NXY selector widget **/
GtkWidget *nxysel_widget; 
/** NXY id # widget **/
GtkWidget *nxycounter_widget;
#endif

/** EXP selector widget **/
GtkWidget *ctrl_widget;  
int lastcard;

/** Pre trigger widget **/
GtkWidget *pre_trg_widget;
/** Post trigger widget **/
GtkWidget *post_trg_widget;
/** Test pulse delay widget **/
GtkWidget *test_pulse_delay_widget;
/** Test Trigger delay widget **/
GtkWidget *test_trg_delay_widget;
/** Exploder working mode selector**/
//GtkWidget *exploder_mode_widget;


/** Main NxyWidget  **/
GtkWidget *nxyreg_widget;
#ifdef GEMEX_MODE
GtkWidget *nxyreg_widget2;
#endif

/** PC name where daemon whould be running **/
GtkWidget *socketname_widget;


int yes = 1;
int no = 0;


#ifdef SHORTCUTS
GtkWidget *sc_input;
GtkWidget *sc_vth;
GtkWidget *sc_ttm;
#endif

#endif
