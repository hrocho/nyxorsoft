/**
 * @file nxyctr.h
 * A.Prochazka
 * laste update 31.5.2016
 * @brief Library of functions used to manipulate nxyter and global registers
 * This is intended to run at Linux on the PC with SFP connection to GEMEX boards
 * update: 27.1.2016 post and pre trigger window readout were not correct 1 bit was missing
 * 			1.4.2016: update for nxyter v2
 * 			31.5.2016 nxy ctrl register correction
 */ 

#include "nxyreg.h"
#include "mbspex/libmbspex.h"

#define I2C_CTRL_A   0x01
#define I2C_COTR_A   0x03
#define GOS_I2C_DWR  0x8010  // i2c data write reg.   addr
#define GOS_I2C_DRR1 0x8020  // i2c data read  reg. 1 addr
#define GOS_I2C_DWR2 0x8040  // i2c data read  reg. 2 addr
#define GOS_I2C_SR   0x8080  // i2c status     reg.   addr
#define REG_SUBMEM_NUM   0xFFFFD8 //num of channels 8

// fd pexor
static int            fd_pex;

// helping functions
int  f_pex_slave_rd (long, long, long, long*);
int  f_pex_slave_wr (long, long, long,  long);
int  f_pex_slave_init (long, long);
void f_i2c_sleep ();
void printm (char *fmt, ...);



/**
 * Opens the pexor FD using mbspex library
 * @return 1 on success and 0 on failure 
 */
int open_pexor(){
	printf("opening pexor device ...\n");
	if ((fd_pex = mbspex_open (0)) == -1){
		return 0;
	}
	return 1;
}

/**
 * Initialize SFPs and nodes specified at globalref greg
 * @param struct globareg *greg
 * @return 1 on success otherwise on failure
 */  
int init_sfps(struct globalreg *greg){
	int i,stat;
	for (i=0; i<MAX_SFP; i++){
	if (greg->sfp_in_use[i][0] > 0){
		stat = f_pex_slave_init (i, get_explo_number(*greg,i));
		if (stat == -1){
			return 0;
			}
		}
	}
	return 1;
}

/**
 * Activates the i2c link to GEMEX/Exploder card specified by sfp and exp parameters
 * This function will not reset the exploder.
 * @param sfp sfp number 
 * @param exp exp number
 * @return 1 on success, exit on failure
 * @warning reset code is hardcoded
 */ 
int i2c_init(int sfp, int exp, int wreset){
	long unsigned  w;
	int res=0;
	#ifdef NXYTER_V2
	//w = greg.nxy_ctrl[sfp][exp]-4;
	w = wreset;
	#else
	w = 0x80;
	#endif
	w+= 0x0              <<  8;
	w+= 0x0              << 16;
	w+= I2C_CTRL_A       << 24;

	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res == -1)
	{
		printf ("ERROR>> activating I2C core failed: \n");
		printf ("        SFP: %d, EXP: %d => A: 0x%x, D: 0x%x\n",
                                sfp, exp, GOS_I2C_DWR, w);
		printf ("exiting.. \n");
		exit (0);
          }
	return 1;
	}

/**
 * Activates the i2c link to GEMEX/Exploder card specified by sfp and exp parameters
 * This function will reset the exploder.
 * @param sfp sfp number 
 * @param exp exp number
 * @return 1 on success, exit on failure
 * @warning reset code is hardcoded
 */ 
int i2c_init_write(int sfp, int exp, int wreset){
	long unsigned  w;
	int res=0;

	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, 0x7f000000);
	if(res == -1)
	{
		printf ("ERROR>> activating I2C core failed: \n");
		printf ("        SFP: %d, EXP: %d => A: 0x%x, D: 0x%x\n",
                                sfp, exp, GOS_I2C_DWR, w);
		printf ("exiting.. \n");
		exit (0);
          }

	#ifdef NXYTER_V2
	//w = greg.nxy_ctrl[sfp][exp];
	w = wreset;
	#else
	w = 0x84;
	#endif
	w+= 0x0              <<  8;
	w+= 0x0              << 16;
	w+= I2C_CTRL_A       << 24;

	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res == -1)
	{
		printf ("ERROR>> activating I2C core failed: \n");
		printf ("        SFP: %d, EXP: %d => A: 0x%x, D: 0x%x\n",
                                sfp, exp, GOS_I2C_DWR, w);
		printf ("exiting.. \n");
		exit (0);
          }
	return 1;
	}

/**
 * This function deactivates the i2c link
 * @param sfp sfp number 
 * @param exp exp number
 * @return 1 on success, exits when failure.
 * @warning exits on falure maybe needs to be changed
 */ 
int i2c_deactivate(int sfp, int exp){
	long unsigned  w;
	int res=0;
	w = 0x0;
	w+= 0x0              <<  8;
	w+= 0x0              << 16;
	w+= I2C_CTRL_A       << 24;

	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res == -1)
	{
		printf ("ERROR>> deactivating I2C core failed: \n");
		printf ("        SFP: %d, EXP: %d => A: 0x%x, D: 0x%x\n",
                                sfp, exp, GOS_I2C_DWR, w);
		printf ("exiting.. \n");
		exit (0);
          }
	return 1;
	}

/**
 * This function reads the nxyter mask register
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function will overwrite the register values.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read_mask(struct nxyreg *r)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;
	int i;

	for(i=0;i<16;i++)
	{
		w = (i					<<8);
		w+=((r->i2c_addr+1)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		r->mask[i]=data&0xff;
		}

	return 1;
	}

/**
 * This function update the nxyters mask register according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_mask(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;
	int i;

	for(i=0;i<16;i++)
	{
		w = r->mask[i];
		w+= (i					<<8);
		w+=((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}


/**
 * This function reads the nxyter bias registers
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function will overwrite the register values.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read_bias(struct nxyreg *r)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;
	int i;

	for(i=0;i<14;i++)
	{
		#ifndef NXYTER_V2 // skip 4 register removed from nxyter v2
		if((i==1) || (i==4) || (i==5) || (i==6) ){
			continue;
		}
		#endif
		w = (i+0x10)			<<8;
		w+=((r->i2c_addr+1)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		r->bias[i]=data&0xff;
		}

	return 1;
	}
/**
 * This function update the nxyters bias register according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_bias(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;
	int i;

	for(i=0;i<14;i++)
	{
		#ifndef NXYTER_V2 // skip 4 register removed from nxyter v2
		if((i==1) || (i==4) || (i==5) || (i==6) ){
			continue;
		}
		#endif
		w = r->bias[i];
		w+= (i+0x10)			<<8;
		w+=((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}


/**
 * This function reads the nxyter config registers
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function will overwrite the register values.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read_config(struct nxyreg *r)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;
	int i;

	for(i=0;i<2;i++)
	{
		w = (i+0x20)			<<8;
		w+= ((r->i2c_addr+1)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		r->config[i]=data&0xff;
		}

	return 1;
	}
/**
 * This function update the nxyters config register according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_config(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;
	int i;

	for(i=0;i<2;i++)
	{
		w = r->config[i];
		w+= (i+0x20)			<<8;
		w+= ((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}

/**
 * This function reads the nxyter test delays registers
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function will overwrite the register values.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read_test_delay(struct nxyreg *r)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;
	int i;

	for(i=0;i<2;i++)
	{
		w = (i+0x26)			<<8;
		w+= ((r->i2c_addr+1)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		r->te_del[i]=data&0xff;
		}

	return 1;
	}

/**
 * This function update the nxyters test delay registers according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_test_delay(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;
	int i;

	for(i=0;i<2;i++)
	{
		w = r->te_del[i];
		w+= (i+0x26)			<<8;
		w+= ((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}

/**
 * This function reads the nxyter clock delay registers
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function will overwrite the register values.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read_clock_delay(struct nxyreg *r)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;
	int i;

	for(i=0;i<3;i++)
	{
		w = (i+0x2b)			<<8;
		w+= ((r->i2c_addr+1)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		r->clk_del[i]=data&0xff;
		}

	return 1;
	}
/**
 * This function update the nxyters clock delay registers according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_clock_delay(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;
	int i;

	for(i=0;i<3;i++)
	{
		w = r->clk_del[i];
		w+= (i+0x2b)			<<8;
		w+= ((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}

/**
 * This function reads the nxyter local thereshold registers
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function will overwrite the register values.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read_th(struct nxyreg *r)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;
	int i;

	w = 0;
	w+= (0x2a)			<<8;
	w+= ((r->i2c_addr+1)	<<16);
	w+=(I2C_COTR_A			<<24);
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
	if(res<0)return res;
	r->thr_te = data&0xff;
	
	
	w = r->thr_te;
	w+= (0x2a)			<<8;
	w+= ((r->i2c_addr)	<<16);
	w+=(I2C_COTR_A			<<24);
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	for(i=0;i<128;i++)
	{
		w = 0;
		w+= (0x2a)			<<8;
		w+= ((r->i2c_addr+1)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		r->thr[i]=data&0xff;
		
		w = r->thr[i];
		w+= (0x2a)			<<8;
		w+= ((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}

/**
 * This function update the nxyters local thresholds registers according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_th(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;
	int i;

	w = r->thr_te;
	w+= (0x2a)			<<8;
	w+= ((r->i2c_addr)	<<16);
	w+=(I2C_COTR_A			<<24);
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	for(i=0;i<128;i++)
	{
		w = r->thr[i];
		w+= (0x2a)			<<8;
		w+= ((r->i2c_addr)	<<16);
		w+=(I2C_COTR_A			<<24);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		}

	return 1;
	}

/**
 * This function update all nxyters registers according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * This function initialize the i2c link before writing and deactivate it afterwards.
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write(struct nxyreg *r)
{
	int res = 0;
	//res = i2c_init_write(r->sfp,r->exp,r->reset);
	//if(res<0)return res;
	res = nxy_write_mask(r);
	if(res<0)return res;
	res = nxy_write_bias(r);
	if(res<0)return res;
	res = nxy_write_config(r);
	if(res<0)return res;
	res = nxy_write_test_delay(r);
	if(res<0)return res;
	res = nxy_write_th(r);
	if(res<0)return res;
	res = nxy_write_clock_delay(r);
	if(res<0)return res;
	#ifdef NXYTER_V2
	res = nxy_write_dacs(r);
	if(res<0)return res;
	#endif
	
	//res = i2c_deactivate(r->sfp,r->exp);
    //if(res<0)return res;
	return 1;
	}

/**
 * This function reads all nxyter registers
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * The function initialize i2c before reading and close it after the reading.
 * The function will overwrite prvided nxyreg structure with values read from nxyters.
 * @param *r is pointer to nxyreg structure, it needs to be allocated and sfp,exp,nxy values properly filled
 * @return 1 on success, else on failure
 * @warning nxyreg structure *r needs to be properly allocated and filled
 */ 
int nxy_read(struct nxyreg *r)
{
	int res = 0;

	res = i2c_init(r->sfp,r->exp,r->reset-4);
	if(res<0)return res;
	res = nxy_read_mask(r);
	if(res<0)return res;
	res = nxy_read_bias(r);
	if(res<0)return res;
	res = nxy_read_config(r);
	if(res<0)return res;
	res = nxy_read_test_delay(r);
	if(res<0)return res;
	res = nxy_read_th(r);
	if(res<0)return res;
	res = nxy_read_clock_delay(r);
	if(res<0)return res;
	#ifdef NXYTER_V2
	res = nxy_read_dacs(r);
	if(res<0)return res;
	#endif
	res = i2c_deactivate(r->sfp,r->exp);
    if(res<0)return res;
	return 1;
	}

/**
 * This function reads the global registers
 * sfp, exp, and nxy number is taken from parametera
 * The function will overwrite the register values.
 * @param sfp sfp number
 * @param exp exp number
 * @param greg pointer to globalreg structure where values will be written
 * @return 1 on success, else on failure
 * @warning global reg structure must be properly allocated
 */ 
int global_read(int sfp, int exp, struct globalreg *greg)
{
	long unsigned  w;
	long unsigned data;
	int res = 0;	

	// control register
	w = 0xa1000000;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
    res = f_pex_slave_rd (sfp, exp, GOS_I2C_DRR1, &data);
    if((data&0xffff0000) != 0x89a10000){
		printf("global_read error: control register = 0x%x (sfp = %d, exp = %d)\n",data,sfp,exp);
		return -2;
		}
    if(res<0)return res;       
    
    if(greg->sfp_in_use[sfp][exp]==1)res = 0x80;
    if(greg->sfp_in_use[sfp][exp]==2)res = 0x88;
    if( (res!=(data&0xff)) && ( (res+4)!=(data&0xff))){
		printf("global_read nxy receiver error: control register = 0x%x (sfp = %d, exp = %d)\n",data,sfp,exp);
		return -3;
		}
    
    #ifdef NXYTER_V2
	greg->nxy_ctrl[sfp][exp] = (data&0x3fff);
    #else
	if((data&0xff)==0x84 || (data&0xff)==0x8c)
    	greg->mode = 1;
	if((data&0xff)==0x80 || (data&0xff)==0x88)
    	greg->mode = 0;    	
    #endif


    //pre trigger window
    w = 0xa2000000;
    res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
    res = f_pex_slave_rd (sfp, exp, GOS_I2C_DRR1, &data);
    if(res<0)return res;
    if((data&0xffff0000) != 0x89a20000)return -4;
    greg->pre_trg_wind = data&0xfff;

    //post trigger window
    w = 0xa3000000;
    res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
    res = f_pex_slave_rd (sfp, exp, GOS_I2C_DRR1, &data);
    if((data&0xffff0000) != 0x89a30000)return -5;
    if(res<0)return res;
    greg->pos_trg_wind = data&0xfff;

    // test pulse delay
    w = 0xa4000000;
    res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
    res = f_pex_slave_rd (sfp, exp, GOS_I2C_DRR1, &data);
    if((data&0xffff0000) != 0x89a40000)return -6;
    if(res<0)return res;
    greg->test_pul_del = data&0xfff;

    // test trigger delay
    w = 0xa5000000;
    res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
    res = f_pex_slave_rd (sfp, exp, GOS_I2C_DRR1, &data);
    if((data&0xffff0000) != 0x89a50000)return -7;
    if(res<0)return res;
    greg->test_trg_del = data&0xfff;

	return 1;
	}


/**
 * This function will setup ADC phase for the GEMEX provided by nxyreg structure from parameter
 * @param *r pointer to nxyreg structure
 * @return 1 on success else on failure
 * @warning *r must be properly adjusted, GEMEX id are taken from values in *r
 */ 
int nxy_adc_setup(struct nxyreg *r)
{
	long unsigned  w;
	int res = 0;

	//activate SPI on exploder
	w = 0x11000080;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	// set SPI speed
	w = 0x12000044;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	//adjust ADC dc0 phase
	w = 0x15001600 | r->adc_dco_phase;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	//deactiva SPI
	w = 0x11000000;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	return 1;
	}

/**
 * Resets the exploder/GEMEX card specified by parameters
 * @param sfp SFP number
 * @param exp EXP number
 * @return 1 on success
 */ 
int exploder_reset(int sfp, int exp)
{
	long unsigned  w;
	int res = 0;

	//activate SPI on exploder
	w = 0x7e000000;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	return 1;
	}

/**
 * Resets the exploder/GEMEX clock specified by parameters
 * @param sfp SFP number
 * @param exp EXP number
 * @return 1 on success
 */ 
int exploder_clock_reset(int sfp, int exp)
{
	long unsigned  w;
	int res = 0;

	//activate SPI on exploder
	w = 0x21000001;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	return 1;
	}

/**
 * Writes the global registers provided by greg parameter to Exploder/GEMEX card specified by parameters
 * @param sfp sfp number
 * @param exp exp number
 * @param greg globalreg structure with values to be updated to Exploder/GEMEX
 * 
 */ 
int global_write(int sfp, int exp, struct globalreg greg)
{
	long unsigned  w;
	int res = 0;

	// write pre trigger window
	w = 0x22000000 + greg.pre_trg_wind;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	// write post trigger window
	w = 0x23000000 + greg.pos_trg_wind;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	// write test pulse delay
	w = 0x24000000 + greg.test_pul_del;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	// write test trigger delay
	w = 0x25000000 + greg.test_trg_del;
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;


	return 1;
	}

/**
 * Set the working mode of the the exploder/GEMEX cars specified by parameters
 * Mode is taken according to globalreg configuration supplied by greg parameter
 * greg.mode = 0 is normal mode, greg.mode = 1 is test mode
 * @param sfp SFP number
 * @param exp EXP number
 * @param greg globalreg structure from where working mode will be taken
 * @return 1 on success
 */ 
int exploder_activate_receiver(int sfp, int exp, struct globalreg greg)
{
	long unsigned  w;
	int res = 0;

	#ifdef NXYTER_V2
		if(greg.sfp_in_use[sfp][exp]==2)w = 0x21000000+greg.nxy_ctrl[sfp][exp];
		if(greg.sfp_in_use[sfp][exp]==1)w = 0x21000000+greg.nxy_ctrl[sfp][exp];
	#else
    if(greg.sfp_in_use[sfp][exp]==1)w = 0x21000080+(greg.mode*4); //84
    if(greg.sfp_in_use[sfp][exp]==2)w = 0x21000088+(greg.mode*4); //8c
    #endif
    
	res = f_pex_slave_wr (sfp, exp, GOS_I2C_DWR, w);
	if(res<0)return res;

	return 1;
	}


/**
 * Readout external DAC outputs from NYXOR cards
  *r is pointer to nxyreg structure
 * @param  *r is pointer to nxyreg structure
 * @return 1 on success
 */ 
int nxy_read_dacs(struct nxyreg *r){
	long unsigned w,data;
	int res = 0;
	int i;
	if(r==NULL)return -1;
	
	// activate i2c for external dacs
	w = 0x1000080 + r->nxy;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;
	
	// reading 4 outputs
	for(i=0;i<4;i++){
		w = 0xbc00000 + (0x100000 * r->nxy) + (0x1000 << i);
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, 0x84000000);
		if(res<0)return res;
		
		res = f_pex_slave_rd (r->sfp, r->exp, GOS_I2C_DRR1, &data);
		if(res<0)return res;
		data = (data>>6)&0x3ff;
		r->ext_dacs[i]=data;
	}
	
	// deactivate i2c
	w = 0x1000000;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;
	return 1;
}

/**
 * This function update the external DACs of the nxyter according to values
 * provided by pointer to nxyreg structure
 * sfp, exp, and nxy number is taken from nxyreg structure provided by parameter
 * @param *r is pointer to nxyreg structure
 * @return 1 on success, else on failure
 */ 
int nxy_write_dacs(struct nxyreg *r){
	long unsigned w;
	int res = 0;
	int i;
	if(r==NULL)return -1;
	
	// activate i2c for external dacs
	w = 0x1000080 + r->nxy;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;
	
	//write to DACs
	for(i=0;i<4;i++){
		w = 0xb430000 + (0x100000 * r->nxy) + (0x1000 << i) + r->ext_dacs[i];
		res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
		if(res<0)return res;
	}
	// deactivate i2c
	w = 0x1000000;
	res = f_pex_slave_wr (r->sfp, r->exp, GOS_I2C_DWR, w);
	if(res<0)return res;
	
	return 1;
}

/**
 * This function will probe localhost PC and try to guess SFP configuration 
 * The globalreg structure will be updated
 * @param struct globareg *greg pointer to global reg structure
 * @return 1  on success, else on failure
 */ 
int nxy_get_global_config(struct globalreg *greg){
	int i,j,status;
	int old_stdout;
	
	old_stdout = dup(1);
	//clear previous config
	memset(greg->sfp_in_use,0,sizeof(greg->sfp_in_use));
	greg->nxy_number = 0;
	// supress error pritout from mbspex functions
	freopen ("/dev/null","w",stdout);
	for(i=0;i<MAX_SFP;i++){
		for(j=0;j<MAX_EXP;j++){
			long unsigned w = 0xa1000000;
			status = f_pex_slave_wr(i,j,GOS_I2C_DWR,0x80);
			status = f_pex_slave_rd (i, j, REG_SUBMEM_NUM, &w);
			if(status!=-1){
				greg->sfp_in_use[i][j]=2;
				greg->nxy_number+=2;
			}
			else break;
		}
	}
	// set stdout back
	fclose(stdout);
	stdout = fdopen(old_stdout,"w");
	return 1;
}



/************* Helping functions ************************************/
/********************************************************************/
int f_pex_slave_init (long l_sfp, long l_n_slaves)
{
  return mbspex_slave_init (fd_pex, l_sfp, l_n_slaves);
}


/********************************************************************/

int f_pex_slave_wr (long l_sfp, long l_slave, long l_slave_off, long l_dat)
{
  int  l_ret;
  l_ret = mbspex_slave_wr (fd_pex, l_sfp, l_slave, l_slave_off, l_dat);
  f_i2c_sleep ();
  return (l_ret);
}

/********************************************************************/

int f_pex_slave_rd (long l_sfp, long l_slave, long l_slave_off, long *l_dat)
{
  int  l_ret;
  l_ret =  mbspex_slave_rd (fd_pex, l_sfp, l_slave, l_slave_off, l_dat);
  f_i2c_sleep ();
  return (l_ret);
}

/********************************************************************/

void f_i2c_sleep ()
{
  #define N_LOOP 500000

  int l_ii;
  int volatile l_depp=0;

  for (l_ii=0; l_ii<N_LOOP; l_ii++)
	{
    l_depp++;
  }
}

void printm (char *fmt, ...)
 {
   char c_str[256];
   va_list args;
   va_start(args, fmt);
   vsprintf (c_str, fmt, args);
   printf ("%s", c_str);
   va_end(args);
 }


