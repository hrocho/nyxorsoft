/**
 * @file
 * A. Prochazka
 * 11.1.2016
 * 
 * This is daemon to control nxyters
 * It is intended to run on a linux computer with SFP access to nxyters
 * headers required: nxyreg.h nxyctrl.h nxysync.h
 * libmpex version
 * last changes:
 * - DUMMY_GEMEX switch added so to run daemon running gemex is not required, 
 *   this is just for debugging purpose
 */ 
#include <stdio.h>
#include "s_veshe.h"
#include "stdarg.h"
#include <sys/file.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include "smem_mbs.h"
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include "./pexor_gosip.h"

#include "sys/socket.h"
#include "sys/un.h"
#include "netinet/in.h"
#include "netdb.h"
#include "arpa/inet.h"

#include <unistd.h>

#include "nxyreg.h"
#include "nxyctrl.h"
#include "nxysync.h"
#include "nxysocket.h"


#define DEBUG 1
//#define DUMMY_GEMEX 1

#define SETUP_FILE "./nxy_nyxor_set.txt"
#define GET_BAR0_BASE     0x1234
#define PEXDEV            "/dev/pexor"
#define PCI_BAR0_NAME     "PCI_BAR0_MBS"
#define PCI_BAR0_SIZE     0x800000


//static s_pexor        sPEXOR;
//static int            fd_pex;
//static int            l_bar0_base;
//static long volatile *pl_virt_bar0;
//static   INTU4  volatile *pl_virt_bar0;
//static long           l_dat1, l_dat2, l_dat3;

int prot, flags;

// globals
struct globalreg greg0;
struct nxyreg *r0=NULL;
struct nxyreg *r=NULL;
struct globalreg greg;
int dolog=0;

// helping functions
//int  f_pex_slave_rd (long, long, long, long*);
//int  f_pex_slave_wr (long, long, long,  long);
//int  f_pex_slave_init (long, long);
//void f_i2c_sleep ();

void terminate(int sig);


// helping structures
struct nxyd_commands{
        int write;
        int read;
        int read_forced;
        int reset;
        int test;
        int quit;
        int end_session;
        int write_confirm;
        int bye;
        int com;
};


/*
 * Create network socket and listen to it
 * function returns socket_fd file descriptor, for later use
 */ 
int nxyd_create_network_socket(){
        int status, yes=1;
        struct addrinfo hints, *serverinfo,*p;
        
        memset(&hints,0,sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;
        status = getaddrinfo(NULL,"3490",&hints, &serverinfo);
        if(status!=0){exit(1);}
        for(p=serverinfo;p!=NULL;p=p->ai_next){
        nxyd_fd = socket(p->ai_family,p->ai_socktype,p->ai_protocol);
        if(nxyd_fd<0){printf("could not open socket\n");continue;}
        if( setsockopt(nxyd_fd,SOL_SOCKET,SO_REUSEADDR, &yes, sizeof(int)) == -1 ){
                        exit(1);
                        }
        status = bind(nxyd_fd,p->ai_addr,p->ai_addrlen);
        if(status<0){printf("could not bind to socket, nxy daemon already running ?\n");exit(1);}
        break;
        }
        if(p==NULL){close(nxyd_fd);exit(1);}
        freeaddrinfo(serverinfo);
        status = listen(nxyd_fd,1);
        if(status!=0){printf("could not listen to socket\n");exit(1);}

        return nxyd_fd;
}

                                                
nxyd_commands_clear(struct nxyd_commands *c){
        memset(c,0,sizeof(struct nxyd_commands));
        c->bye = 1;
}

nxyd_commands_print(struct nxyd_commands *c){
        int com  = c->com;
        printf("*** command: %c %x %x %d ***\n",(com&0xff000000)>>24,(com&0xff0000)>>16,(com&0xff00)>>8,(com&0xff));
        printf("activated operations: ");
        if(c->write)printf("writing ");
        if(c->write_confirm)printf("writing with confirmation");
        if(c->read)printf("reading ");
        if(c->read_forced)printf("reading from hardware ");
        if(c->reset)printf("resetting ");
        if(c->test)printf("test mode ");
        if(c->bye)printf("bye ");
        printf("\n");
}

nxyd_commands_parse(struct nxyd_commands *c, int com){
        c->com = com;
        switch(((com&0xff000000)>>24))
        {
                case 'R': c->read=1;c->read_forced=1;break;
		case 'r': c->read=1;break;
		case 'F': c->read=1;c->read_forced=1;break;
                case 'w': c->write=1;break;
		case 'W': c->write=1;c->write_confirm=1;break;
                case 'T': c->test = 1;c->write=1;break;
                case 'Q': c->quit=1;break;
		case 'E': c->end_session = 1;break;
                default: c->write=0;c->read=0;break;
            }

            switch(((com&0xff0000)>>16))
            {
                case 255: c->reset=1;break;
                default:  c->reset=0;break;
            }

		/*
            switch(((com&0xff00)>>8))
            {
                case 255: c->bye=0;break;
                default: c->bye=1;break;
            }
		*/
}

int nxyd_read_from_hardware(){
        int status, i, length;
        char *save;
        
        
        #ifdef DEBUG
                printf("Retrieving config from nxyters ...\n");
	#endif
	
        
        //if(nxyregcpy(&r, &r0,&greg, &greg0)==NULL)return 0;
                
        for(i=0;i<greg0.nxy_number;i++){
                status = nxy_read(&r[i]);
		if(status<1){printf("nxy reading error!\n");return 0;}
                if(r[i].nxy==0){
                        status = global_read(r[i].sfp,r[i].exp,&greg);
                        if(status<1){printf("global reading error (%d)!\n",status);return 0;}
                        }
		
               	}
		save = nxydata2str(greg,r,&length);
		memcpy(nxy_shm, save, length); // save to shared memory
		free(save);
                return 1;
}

int nxyd_confirm_settings(struct nxyreg *_r, struct globalreg *_g){
        struct globalreg g1;
        struct nxyreg *r1=NULL;
        char *save;
        int length;
        int status;
        int i;
        
        nxyregcpy(&r1, &_r, &g1, _g);
        for(i=0;i<greg0.nxy_number;i++){
                status = nxy_read(&r1[i]);
                if(status<1){printf("nxy reading error!\n");free(r1);return 0;}
		if(r[1].nxy==0){
			status = global_read(r1[i].sfp,r1[i].exp,&g1);
			if(status<1){printf("global reading error (%d)!\n",status);free(r1);return 0;}
			}
		}
                
                save = nxydata2str(g1,r1,&length);
                memcpy(nxy_shm, save, length);
                free(save);
                
		if(memcmp(&g1,_g,sizeof(struct globalreg) - sizeof(REG_INT_TYPE))!=0){
			printf("global registers not confirmed!\n");
			print_global(g1);
			print_global(*_g);
			free(r1);
			return 0;
			}
			
		if(memcmp(_r,r1,greg0.nxy_number*sizeof(struct nxyreg))!=0){
                        printf("nxyter registers not confirmed!\n");
			#ifdef DEBUG
			for(i=0;i<greg0.nxy_number;i++){
				status = memcmp(&_r[i],&r1[i],sizeof(struct nxyreg));
				if(status!=0){
					printf("nxyter %d is not the same(%d)\n",i,status);
					printf("Read:\n");
					print_nxy_reg(r1[i]);
					printf("Write:\n");
					print_nxy_reg(_r[i]);
				}
			}
			#endif
                        free(r1);return 0;
                
                }
                #ifdef DEBUG
                printf("confirmed\n");
                #endif
        free(r1);
        return 1;
}


int nxyd_write_settings(struct nxyd_commands *c){
        int i;
        int status;
        int aok=1;

        if(check_sfp_configuration(greg,greg0)==0){ //check if global config is the same
                printf("global configuration not compatible wit current setting\n");
                c->bye=1;
                c->quit=1;
                return 0;
                }
                        
        #ifdef DEBUG2
                printf("Writing nxy registers...\n");
		printf("config:\n");
		print_global(greg);
		for(i=0;i<greg.nxy_number;i++){
			print_nxy_reg(r[i]);
			}

        #endif

        for(i=0;i<greg.nxy_number;i++){ //loop over nxyters
                if(c->reset==0)
                        status = i2c_init(r[i].sfp,r[i].exp,r[i].reset-4);
                else
                        status = i2c_init_write(r[i].sfp,r[i].exp,r[i].reset);

                    if(status<1){printf("writing init nxy error!\n");aok=0;break;}

                    if(c->com & NXY_MASK){
                        status = nxy_write_mask(&r[i]);
                        if(status<1){printf("writing mask nxy error!\n");aok=0;}
                    }

                    if(c->com & NXY_BIAS){
                        status = nxy_write_bias(&r[i]);
                        if(status<1){printf("writing bias nxy error!\n");aok=0;}
                    }

                    if(c->com & NXY_CONFIG){
                        status = nxy_write_config(&r[i]);
                        if(status<1){printf("writing config nxy error!\n");aok=0;}
                    }

                    if(c->com & NXY_CLOCK_DELAY){
                        status = nxy_write_test_delay(&r[i]);
                        if(status<1){printf("writing test delay nxy error!\n");aok=0;}
                    }
                    if(c->com & NXY_CLOCK_DELAY){
                        status = nxy_write_clock_delay(&r[i]);
                        if(status<1){printf("writing nxy error!\n");aok=0;}
                    }
		    if(c->com & NXY_TH){
                        status = nxy_write_th(&r[i]);
                        if(status<1){printf("writing th nxy error!\n");aok=0;}
                    }
		    
		    if(c->com & NXY_EXT_DACS){
                        status = nxy_write_dacs(&r[i]);
                        if(status<1){printf("writing nxy error!\n");aok=0;}
                    }

                    status = i2c_deactivate(r[i].sfp,r[i].exp);                    
                    if(status<1){printf("writing: i2c deactivation error!\n");aok=0;}

					
                if(r[i].nxy==1 && c->reset==0){						
			if(c->com & NXY_GLOBAL){
                                status = global_write(r[i].sfp,r[i].exp,greg);
                        if(status<1){printf("writing exp error!\n");aok=0;}
			  	  }
						
			if(c->com & NXY_RECEIVER){
				if(c->test==1)greg.mode = 1;
                                        status = exploder_activate_receiver(r[i].sfp,r[i].exp,greg);
                                        if(status<1){printf("writing exp error!\n");aok=0;}
                                        }
                        }
	
		if(r[i].nxy==1 && c->reset==1){
			status = nxy_adc_setup(&r[i]);
			if(status<1){printf("writing nxy error!\n");aok=0;}
	                       
			status = exploder_reset(r[i].sfp,r[i].exp);
			if(status<1){printf("writing exp error!\n");aok=0;}

			status = exploder_clock_reset(r[i].sfp,r[i].exp);
			if(status<1){printf("writing exp error!\n");aok=0;}

			status = global_write(r[i].sfp,r[i].exp,greg);
			if(status<1){printf("writing exp error!\n");aok=0;}

			if(c->test==1)greg.mode = 1;
			status = exploder_activate_receiver(r[i].sfp,r[i].exp,greg);
			if(status<1){printf("writing exp error!\n");aok=0;}
                    	}
             
						// status = nxy_write(r[i].sfp,r[i].exp,r[i].nxy,&r[i],com&0xff);
						// if(status<1)printf("writing reading error!\n");

           	  } // end of nxyter loop
           	
                
		#ifdef DEBUG
		printf("Nxyter test mode = %d\n",greg.mode);
		#endif

                if(c->write_confirm){/// read out the setting back and compare with what it is suppose to be
                        aok = nxyd_confirm_settings(r,&greg);
                        }

                //usleep(20000);
                if(aok==1){
                        return 1;
                        }
                else{
			printf("write error\n");
			c->bye=1; //close the session after error
                        return -1;
                        }        
        
}


int nxyd_do(struct nxyd_commands *c){
        struct nxyreg *r1;
        struct globalreg greg1;
        struct timespec ts;
        int waittime = 20;
        time_t curtime;
        int status;
        int length;
        char *buf=NULL;


        #ifdef DEBUG
                nxyd_commands_print(c);
        #endif
        //manual tuning of commands
        if(c->quit==1){c->bye = 1;} //quit the daemon
        if(c->end_session==1){c->bye=1;}
        if(c->reset==1)c->com = c->com|255; // if reset is required update all registers
        
        if(c->write == 1){
                r1 = str2nxydata(nxy_reply.msg,&greg1);
		#ifdef DEBUG2
		printf("received config:\n");
		print_global(greg1);
		for(i=0;i<greg1.nxy_number;i++){
			print_nxy_reg(r1[i]);
			}

		#endif
		
                if(r1==NULL){
                        c->bye=1;
                        return -1;
                        }
                nxyregcpy(&r,&r1,&greg,&greg1);
		free(r1);
                }
        /// going to lock nxyter if needed
        if(c->write||c->read_forced){ // waiting for nxyter to be ready to read or write
                #ifdef DEBUG
                        printf("waiting to for lock ... \n");
                #endif
			
                clock_gettime(CLOCK_REALTIME,&ts);
                ts.tv_sec+=waittime;
                status = sem_timedwait(nxy_mutex,&ts); //nxy locking
                if(status!=0){
                        nxyd_commands_clear(c);
                        c->bye=1;
                        nxy_unlock();
                        return -22;
                        }
                }
        
        /// we are going to write to nxyters
        if(c->write){ ///writing nxy registers
                #ifndef DUMMY_GEMEX
                if(nxyd_write_settings(c)==1){
                        if(dolog==1 && greg.mode==0){ // log setting to txt file with timestamp
                                struct tm * timeinfo;
                                char flogname[23];
				time(&curtime);
                                timeinfo = localtime (&curtime);
                                strftime (flogname,23,"nxy_%y_%m_%e_%R.txt",timeinfo);
                                status = save_nxy_file(flogname,greg,r);
                                }
                }
                else{
                        nxyd_commands_clear(c);
                        c->bye=1;
                        nxy_unlock();
                        return -2;
                        }
                #endif
                nxy_unlock();
                return 1;
	} //end of if data_write
        
        
        if(c->read){
                if(c->read_forced == 1){
                        #ifndef DUMMY_GEMEX
                        if(nxyd_read_from_hardware()!=1){
                                nxy_unlock();
                                return -3;
                                }
                        #endif
                        }
                
                if(nxy_connection.reply_type==NXYD_PROTOCOL){
                        length = sizeof(struct globalreg) + (greg.nxy_number * sizeof(struct nxyreg));
                        #ifdef DEBUG
                        printf("structure size =  %d\n",length);
                        #endif                
                        buf = pack(c->com,&length,nxy_shm);
                        if(buf==NULL){nxy_unlock();return -4;}
                        memcpy(nxy_reply.msg,buf,length);
                        nxy_reply.len = length;
                        free(buf);
                        
                        #ifdef DEBUG
                        printf("reply prepared, length = %d\n",nxy_reply.len);
                        #endif
                        }
                #ifndef NXYDAEMON_NO            
                if(nxy_connection.reply_type==HTTP_PROTOCOL){
                        char *buf = (char *)malloc(20000);
                        buf = save_nxy_str(buf,greg,r);
                        if(buf!=NULL){
                                nxy_reply.len = strlen(buf);
                                strcpy(nxy_reply.msg,buf);
                                nxy_reply.len = strlen(nxy_reply.msg);
                                #ifdef DEBUG
                                printf("reply len = %d\n",nxy_reply.len);
                                printf("reply:\n%s\n",nxy_reply.msg);
                                #endif
                                }
                        else{
                                nxy_unlock();
                                nxy_reply.len = 0;
                                return -5;
                        }
                }
                #endif
                                        
                nxy_unlock();
                return 2;
        } //end of data_read
        
        
        
        
}

nxyd_help(){
	printf("Nxyter Daemon usage:\n");
	printf("./nxyd -option [argument]\n");
	printf("available options:\n");
	printf("  -s or --skipsetup skip the initialization of the nxyters\n");
	printf("  -c or --config [filename] load configuration from file filename\n");
	printf("  -b or --background run the daemon in background\n");
	printf("  -l or --log log nxyters setting when changed to file with timestamp\n");
	printf("  -r or --reset reset the nxyters and clock after the initial setup'n");	
	printf("  -h or --help prints help message\n");
	printf("  -q or --quit kill the nxyter daemon\n");
	printf("\nexample:\n");
	printf("./nxyd --config nxy_config.txt --reset\n");
}


////////////////// main function //////////////////////////////////
int main(int argc, char **argv)
{
char *filename=SETUP_FILE;
char *buf=NULL,*save=NULL;
int status, length;
int i=0,background=0, dosetup=1, doquit=0,doreset=0;
pid_t pid, sid;
FILE *fout;

/// ctrl-c interuption
struct sigaction saction;
saction.sa_handler = terminate;
sigemptyset(&saction.sa_mask);
saction.sa_flags = 0;
sigaction(SIGINT,&saction,NULL);
	

nxy_clear_ipc();
status = nxy_init_ipc();
if(status !=1){
	perror("nxy ipc init");
	exit(1);
	}
memset(&greg0,0,sizeof(greg0));

/// Argument parsing
if(argc>1){
	for(i=1;i<argc;i++)
	{
		if( (strcmp(argv[i],"-c")==0) || (strcmp(argv[i],"--config")==0)){
			if((i+1)>=argc){printf("wrong arguments\n");exit(1);}
			filename = argv[i+1];
			i++;
			}
			
		if((strcmp(argv[i],"-b")==0) || (strcmp(argv[i],"--background")==0)){
			background = 1;
			}

                if((strcmp(argv[i],"-s")==0) || (strcmp(argv[i],"--skipsetup")==0)){
			dosetup = 0;
                }
		
		if((strcmp(argv[i],"-r")==0) || (strcmp(argv[i],"--reset")==0)){
			doreset = 1;
                }
            
                if((strcmp(argv[i],"-l")==0) || (strcmp(argv[i],"--log")==0)){
			dolog = 1;
                }
            
                if((strcmp(argv[i],"-q")==0) || (strcmp(argv[i],"--quit")==0)){
			doquit = 1;
                }
		
		if((strcmp(argv[i],"-h")==0) || (strcmp(argv[i],"--help")==0)){
			nxyd_help();
			return 0;
                }
		
		}    
    i=0;
}

///kill the existing daemon
if(doquit){
	connect_daemon("localhost");
	buf = pack((int)'Q'<<24,&length,NULL);
	if(buf==NULL){
		printf("error, exiting...\n");
		}
	status = sendall(nxyd_fd,buf,&length);
	if(status!=1){
		printf("Could not send kill signal to the daemon, exiting ...\n");
		}
	free(buf);
    close(nxyd_fd);
    return 1;
	}

/// connecting to network socket
nxyd_create_network_socket();


/// loading settings from asci file
printf("loading %s\n",filename);
r0 = load_nxy_file(filename,&greg0);
if(r0==NULL){printf("could not load config file\n");exit(0);}
//free(r0);
print_global(greg0);

for(i=0;i<greg0.nxy_number;i++){
	struct nxyreg rr;
	nxy_id(i,&rr.sfp,&rr.exp,&rr.nxy,greg0);
	printf("i = %d, %d %d %d\n",i,rr.sfp,rr.exp,rr.nxy);
}

// initializing variables
if(nxy_reply_msg_init(greg0.nxy_number)==NULL){
        printf("Could not allocate reply buffer\n");
        exit(1);
};
nxy_reply_msg_clear();
nxy_connection_init(&nxy_connection,&nxy_reply);


///forking
if(background == 1){
	printf("going to background ...\n");
	pid = fork();
	if(pid!=0){
		exit(1);
		}
	umask(0);
	sid = setsid();
	if(sid<0){printf("error while forking, exiting ...\n");exit(1);}
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
        fout = freopen("nxyd.log", "w+", stdout);
	}

///init pexor
if( open_pexor() != 1){
	printf("ERROR>> could not open mbspex device \n");
	exit(0);
	}

#ifndef DUMMY_GEMEX
printf("initializing SFPS...\n");
// initializing SFPs
status = init_sfps(&greg0);
if(status!=1){
	printf ("ERROR>> could not initialize SFPs\n");
	exit (0);
	}
#else
printf("Using dummy gemex\n");
#endif

/// initializing settings before running the daemon
#ifndef DUMMY_GEMEX
if(dosetup){
	printf("setting up nxyters ...\n");
	for(i=0;i<greg0.nxy_number;i++){
		if(doreset==1){
			status = i2c_init_write(r0[i].sfp,r0[i].exp,r0[i].reset);
		}
		else{
			status = i2c_init(r0[i].sfp,r0[i].exp,r0[i].reset-4);
		}
		if(status<0)exit(1);
		
		status = nxy_write(&r0[i]);
		if(status<1){printf("writing nxy error!\n");exit(1);}
		
		status = nxy_adc_setup(&r0[i]);
		if(status<1){printf("writing nxy error!\n");exit(1);}
                
		if(doreset){
			status = exploder_reset(r0[i].sfp,r0[i].exp);
			if(status<1){printf("writing exp error!\n");exit(1);}

			status = exploder_clock_reset(r0[i].sfp,r0[i].exp);
			if(status<1){printf("writing exp error!\n");exit(1);}
		}
		
                status = global_write(r0[i].sfp,r0[i].exp,greg0);
                if(status<1){printf("writing exp error!\n");exit(1);}

                status = exploder_activate_receiver(r0[i].sfp,r0[i].exp,greg0);
		if(status<1){printf("writing exp error!\n");exit(1);}
		
		status = i2c_deactivate(r0[i].sfp,r0[i].exp);
			if(status<0)exit(1);
		}
		
		
		
		printf("initialized nxyters to: \n");
		for(i=0;i<greg0.nxy_number;i++){
			status = nxy_read(&r0[i]);
			if(status<1){printf("reading back nxy error!\n");exit(1);}
			status = global_read(r0[i].sfp,r0[i].exp,&greg0);
			if(status<1){printf("reading back global error!\n");exit(1);}						
			print_nxy_reg(r0[i]);
		}
		printf("\nglobal settings:\n");
		print_global(greg0);
		
	}
#endif
nxyregcpy(&r,&r0,&greg,&greg0);
save = nxydata2str(greg,r,&length);
memcpy(nxy_shm, save, length); // save to shared memory
free(save);
save=NULL;

/// main loop
while(1)
{
        struct nxyd_commands comdata;

        
        printf("-------------- waiting for new connection -----------------\n");
        status = nxy_connection_accept(&nxy_connection);
        if(status != 1){
                comdata.bye=-1;
                }
        nxyd_commands_clear(&comdata);
        nxyd_commands_parse(&comdata, nxy_connection.com);
        status = nxyd_do(&comdata);
        
        if(status<1){
                printf("error (%d)\n",status);
		nxy_connection_error(&nxy_connection);
		comdata.bye=-1; //close the session after error
                }

            if(status==2){ // this is after read and data need to be send back
                length = nxy_reply.len;
                #ifdef DEBUG
                printf("sending data (%d bytes)...\n",nxy_reply.len);
                #endif
                nxy_connection_send(&nxy_connection);
                if(status<1){printf("could not send data\n");}                
            }
            
	if(comdata.bye ==1){
		nxy_connection_OK(&nxy_connection);
                }
        
	#ifdef DEBUG
        printf("closing session...\n");
	#endif
        nxy_connection_close(&nxy_connection);
        
        if(comdata.quit==1){
	     break;
        }
} // end of main loop


printf("exiting ... \n");
free(r0);
free(r);
close(nxyd_fd);
nxy_clear_ipc();
return 0;
}


void terminate(int sig){
	printf("exiting ...\n");
	nxy_clear_ipc();
	exit(1);

}


