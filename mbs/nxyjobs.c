/**
 * @file
 * A. Prochazka 
 * 13.7.2013
 * 
 * set of function used for calibration, readout and manipulation of nxyters 
 * from MBS
 */
  
#include "nxyjobs.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

void nxydata_fill(int _sfp,int _exp, int _nxy, int _ch, int _adc);
int nxydata_unpack(long *pdata);
double nxyint_mean(int *b);
double nxyint_median(int *b);
double nxyhits_mean(struct nxyhits *n, int ch);
double nxyhits_median(struct nxyhits *n, int ch);
double nxyhits_nhitmean(struct nxyhits *n);
int compare(const void *a, const void *b);
int nxyint_maxindex(int *b, int num);
int nxyint_closestindex(int *b, int aim, int num);
double nxyint_rms(int *b, int num);
int nxyresult_maxindex(int num, int nxy);
int nxyresult_closestindex(double aim, int num, int nxy);



int nxyresult_allocate(int num){
	//if(nxyresult=!NULL){
	//		nxyresult_free(nxyresult);
	//	}
	nxyresult = (struct nxyres2*)malloc(nxydata.nxy_number*num*sizeof(struct nxyres2));
	if(nxyresult==NULL){
		printm("Could not allocate memory\n");
		exit(0);
	}
	//printm("Result array allocated for %d %d at %d\n",nxydata.nxy_number,num,nxyresult);
	memset(nxyresult,0,nxydata.nxy_number*num*sizeof(struct nxyres2));
	return 1;
}

void nxyresult_free(){
	//printm("Freeing nxy result %d ",nxyresult);
	free(nxyresult);
	nxyresult=NULL;
}

#define nxyresult_at_ch(i,n,ch) nxyresult[(i)*nxydata.nxy_number+(n)].data[(ch)]
#define nxyresult_at_val(i,n) nxyresult[(i)*nxydata.nxy_number+(n)].val
#define nxyresult_at(i,n) nxyresult[(i)*nxydata.nxy_number+(n)]


/////////////////// Nxy data initializtion /////////////////////////////////
int nxydata_init(char *_socket_name){
	#ifdef DEBUG
	printm("nxydata_init: initializing data\n");
	#endif
	_r = NULL;
	
	nxy_socket_name = _socket_name;
	nxydata.nxy_number = _greg0.nxy_number;
	nxydata.phits = (struct nxyhits*)malloc(nxydata.nxy_number*sizeof(struct nxyhits));
	if(nxydata.phits==NULL)return 0;
	nxyresult = NULL;

	nxydata_event_counter = 0;
	nxydata_ready = 0;
	nxydata_status = 0;
	nxydata_job_stage = 0;
	nxyjob_parameter = 1;
	memset(&nxyjobs,0,sizeof(struct _nxyjobs));
	#ifdef DEBUG
	printm("nxydata_init: allocated for %d nxyters\n",nxydata.nxy_number);
	#endif
	return 1;
}

//////////// clean initialized data ///////////////
int nxydata_clean(){
	free(nxydata.phits);
	return 1;
}


///////////// reset nxy data /////////////////
void nxydata_reset(){
	memset(nxydata.phits,0,nxydata.nxy_number*sizeof(struct nxyhits));
	nxydata_event_counter=0;
	nxydata_ready=0;
}



///////////// nxyjob_set //////////////////
int nxyjob_set(){
	nxyjobs_t d;
	int i;

	
	d = nxyjobs.q[0];	
	if(d==0){
		nxydata_job_stage=0;
		return 0;
		}
	#ifdef DEBUG
	printm("\n------------ nxyjob_set: switching to job %d\n",d);
	#endif
	
	nxyjob_parameter = nxyjobs.par[0]; //update actual parameter
	switch(d){

		case NXYJOB_VBIASS_SCAN : {
					nxyjob_function_init = &nxyjob_vbiass_init;
					nxyjob_function_main = &nxyjob_vbiass_main;
					nxyjob_function_done = &nxyjob_vbiass_done;
					nxydata_job_stage=1;
					break;
					}
					
		case NXYJOB_VBIASS_SCAN_AT : {
					nxyjob_function_init = &nxyjob_vbiass_at_init;
					nxyjob_function_main = &nxyjob_vbiass_at_main;
					nxyjob_function_done = &nxyjob_vbiass_at_done;
					nxydata_job_stage=1;
					break;
					}
		
		case NXYJOB_VBIASS2_SCAN : {
					nxyjob_function_init = &nxyjob_vbiass2_init;
					nxyjob_function_main = &nxyjob_vbiass2_main;
					nxyjob_function_done = &nxyjob_vbiass2_done;
					nxydata_job_stage=1;
					break;
					}
					
		case NXYJOB_VBIAS_SCAN_BOTH : {
					nxyjob_function_init = &nxyjob_vbiasboth_init;
					nxyjob_function_main = &nxyjob_vbiasboth_main;
					nxyjob_function_done = &nxyjob_vbiasboth_done;
					nxydata_job_stage=1;
					break;
					}
					

		case NXYJOB_VTH_SCAN : {
					nxyjob_function_init = &nxyjob_vth_init;
					nxyjob_function_main = &nxyjob_vth_main;
					nxyjob_function_done = &nxyjob_vth_done; 	
					nxydata_job_stage=1;	
					break;
					}
		case NXYJOB_LOCALTH_SCAN : {
					nxyjob_function_init = &nxyjob_lth_init;
					nxyjob_function_main = &nxyjob_lth_main;
					nxyjob_function_done = &nxyjob_lth_done; 	
					nxydata_job_stage=1;	
					break;
					}
		case NXYJOB_LOCALTH_EQ : {
					nxyjob_function_init = &nxyjob_ltheq_init;
					nxyjob_function_main = &nxyjob_ltheq_main;
					nxyjob_function_done = &nxyjob_ltheq_done; 	
					nxydata_job_stage=1;	
					break;
					}
					
		case NXYJOB_BASELINES_READ : {
					nxyjob_function_init = &nxyjob_baselines_init;
					nxyjob_function_main = &nxyjob_baselines_main;
					nxyjob_function_done = &nxyjob_baselines_done; 	
					nxydata_job_stage=1;	
					break;
					}
         case NXYJOB_INTERNAL_CALIBRATION : {
					nxyjob_function_init = &nxyjob_internal_calibration_init;
					nxyjob_function_main = &nxyjob_internal_calibration_main;
					nxyjob_function_done = &nxyjob_internal_calibration_done; 	
					nxydata_job_stage=1;	
					break;
					}
		 case NXYJOB_SAVE_CONFIG:{
					nxyjob_function_init = NULL;
					nxyjob_function_main = NULL;
					nxyjob_function_done = &nxyjob_save_config_done;
					nxydata_job_stage=3;	
					break;
		 }
                
		default: return 0;	
		}

	for(i=1;i<MAX_JOBS;i++){
		if(nxyjobs.q[i]==0){
			nxyjobs.q[i-1]=0;
			nxyjobs.par[i-1]=0;
			break;
			}
		nxyjobs.q[i-1]=nxyjobs.q[i];
		nxyjobs.par[i-1]=nxyjobs.par[i];
	}

	return 1;
}



//////////  nxyjob_process    ////////////
int nxyjob_process(long *_pd){
	if(nxydata_job_stage>0){
		if(nxydata_job_stage == 2 && nxydata_ready==0){
			nxydata_unpack(_pd);
		}
		else{ //action after data were collected 
			if(nxydata_job_stage == 1){
				if(nxyjob_function_init()==1){
					nxydata_job_stage=2;
					nxydata_reset();						
					return 1;
					}
				}

			if(nxydata_job_stage == 2 ){
				if(nxyjob_function_main()){
					nxydata_job_stage = 3;	
					}
				else{
					nxydata_reset();
					}
				}
			
			if(nxydata_job_stage == 3){
				if(nxyjob_function_done() == 1){
					nxyjob_set();
					}
				}
			}
		}
		
	return 1;
}


////////// nxyjob_add //////////////
int nxyjob_add(nxyjobs_t j, int par){
	int i;
	
	for(i=0;i<MAX_JOBS;i++){
		if(nxyjobs.q[i]==0)break;
		}
	if(i>=MAX_JOBS)return 0;
	nxyjobs.q[i]=j;
	nxyjobs.par[i]=par;
	if(i==0 && nxydata_job_stage==0)nxyjob_set();
	return 1;
}

////////////////////////////////////////////////////////////////////////
//////////////////////// Calibration functions definitions /////////////
////////////////////////////////////////////////////////////////////////


//////////////////////// Vbias scan functions //////////////////////////
int nxyjob_vbiass_init(){
	int i;
	int max;
	
	max=0x1ff;
	
	// if supplied parameter is more than 255, lets assume its upper limit of scan
	if(nxyjob_parameter>0xff){
		max = nxyjob_parameter;
	}
	else{ // otherwise take 512 as default value
		nxyjob_parameter = 0x1ff;
	}
	
	nxyresult_allocate(max+1);
	
	#ifdef DEBUG
	printm("VBiaS scan initializing \n");
	printm("scanning up to value %d \n",max);
	#endif
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		#ifdef NXYTER_V2
		_r[i].ext_dacs[2]=1;
		#else
		_r[i].bias[5]=1;
		#endif
							
		_r[i].config[0]=(_r[i].config[0]&0xf0) + 8; //switch to test trigger mode
		printm("nxy%d config reg1 set to %d\n",i,_r[i].config[0]);
	}
	_greg.mode = 1;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);	
	close_daemon();	
	return i;
} 

int nxyjob_vbiass_main(){
	int i,j;
	int c;
	int max;
	max = nxyjob_parameter;

	#ifdef NXYTER_V2
	c = _r[0].ext_dacs[2];
	#else
	c=_r[0].bias[5];
	#endif
	
	#ifdef DEBUG
	printm("VBiaS scan: scanning VbiasS = %d",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			nxyresult_at_ch(c,i,j) = (int)nxyhits_median(&nxydata.phits[i],j);
			}
		nxyresult_at_val(c,i) = nxyint_mean(nxyresult_at(c,i).data);
		#ifdef DEBUG
		printm("nxy%d = %lf\n",i,nxyresult_at_val(c,i));
		#endif
		#ifdef NXYTER_V2
		_r[i].ext_dacs[2]+=1;
		#else
		_r[i].bias[5]++;					
		#endif
	}
	
	#ifdef NXYTER_V2
	if(c>=max){ //check if range is reached (1023 for ext. dac)
		return 1;
	}
	#else
	if(c>=255){ //check if range is reached
		return 1;
	}
	#endif
	
	connect_daemon(nxy_socket_name);
	#ifdef NXYTER_V2
	i = update_daemon(_r,_greg,NXY_EXT_DACS);
	#else
	i = update_daemon(_r,_greg,NXY_BIAS);
	#endif
	

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)
			#ifdef NXYTER_V2
			_r[i].ext_dacs[2]--;
			#else
			_r[i].bias[5]--;
			#endif
	}	
	close_daemon();	
	return 0;
}

int nxyjob_vbiass_done(){
	int i,j,k,c=0;
	FILE *fw,*fw2;
	int max;
	double val;
	max= nxyjob_parameter;
	
	#ifdef DEBUG
	printm("VBiaS scan finished.\n");
	#endif
	
	fw = fopen("dvbias_means.txt","w");
	fw2 = fopen("dvbias_all.txt","w");
	
	
	for(i=0;i<_greg0.nxy_number;i++){
		int dif = 0;
		int pol = 0;
	
		if((_r0[i].config[1]&4)==4)pol=1; //check polarity setting
		#ifdef DEBUG
		if(pol==1)printm("configuring for positive polarity\n");
		else printm("configuring for negative polarity\n");
		#endif
		
		/// scan through values and check where its start to increase
		for(j=max-1;j>0;j--){
			if(pol==1)k = max-j;
			else k=j;
			
			val = nxyresult_at_val(k,i);
			
			if(nxyresult_at_val(k,i)!=0 && nxyresult_at_val(k+1,i)!=0){
				dif = nxyresult_at_val(k,i)-nxyresult_at_val(k+1,i);
				if(pol==1)dif=-dif; //to have the same difference sign as negative polarity
				}
			else dif=0;
			
			if(dif<-5){
				j--;
				if(pol==1)k = max-j;
				else k=j;
				dif = nxyresult_at_val(k,i)-nxyresult_at_val(k+1,i);
				if(pol==1)dif=-dif; 
				if(dif<-5){c=k;break;} ///2nd difference bigger than 5, lets take the value
				}
		}
		#ifdef DEBUG
		printm("VbiasS: nxy%d -> value = %d\n",i,c);
		#endif
		#ifdef NXYTER_V2
		_r0[i].ext_dacs[2]=c;
		#else
		_r0[i].bias[5]=c;
		#endif

		/// writes results to files
        for(k=0;k<128;k++)
		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw2,"%d %d %d %d %d %d %d\n",i,_r0[i].sfp,_r0[i].exp,_r0[i].nxy,k,j,nxyresult_at_ch(j,i,k));
				}
			}

		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw,"%d %d %d %d %d %lf\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy,j,nxyresult_at_val(j,i));
				}
			}
			
	}//nxy loop
	fclose(fw);				
	fclose(fw2);
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);
	close_daemon();
	
	free(_r);
	_r=NULL;
	nxyresult_free();
	
	return i;
}


//////////////////////// VbiasS2 scan functions //////////////////////////
int nxyjob_vbiass2_init(){
	int i;
	int max;
	
	max=0x1ff;
	
	// if supplied parameter is more than 255, lets assume its upper limit of scan
	if(nxyjob_parameter>0xff){
		max = nxyjob_parameter;
	}
	else{ // otherwise take 512 as default value
		nxyjob_parameter = 0x1ff;
	}
	
	nxyresult_allocate(max+1);
	
	#ifdef DEBUG
	printm("VBiaS2 scan initializing \n");
	printm("scanning up to value %d \n",max);
	#endif
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].ext_dacs[3]=1;
		_r[i].config[0]=(_r[i].config[0]&0xf0) + 8; //switch to test trigger mode
	}
	_greg.mode = 1;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);	
	close_daemon();	
	return i;
} 

int nxyjob_vbiass2_main(){
	int i,j;
	int c;
	int max;
	max = nxyjob_parameter;

	c = _r[0].ext_dacs[3];
	
	#ifdef DEBUG
	printm("VBiaS2 scan: scanning VbiasS2 = %d",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			nxyresult_at_ch(c,i,j) = (int)nxyhits_median(&nxydata.phits[i],j);
			}
		nxyresult_at_val(c,i) = nxyint_mean(nxyresult_at(c,i).data);
		
		#ifdef DEBUG
		printm("nxy%d = %lf\n",i,nxyresult_at_val(c,i));
		#endif
		
		_r[i].ext_dacs[3]+=1;
	}
	
	if(c>=max){ //check if range is reached (1023 for ext. dac)
		return 1;
	}	
	
	connect_daemon(nxy_socket_name);
	#ifdef NXYTER_V2
	i = update_daemon(_r,_greg,NXY_EXT_DACS);
	#else
	i = update_daemon(_r,_greg,NXY_BIAS);
	#endif
	

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)
			_r[i].ext_dacs[3]--;
	}	
	close_daemon();	
	return 0;
}

int nxyjob_vbiass2_done(){
	int i,j,k,c=0;
	FILE *fw,*fw2;
	int max;
	double val;
	max= nxyjob_parameter;
	
	#ifdef DEBUG
	printm("VBiaS2 scan finished.\n");
	#endif
	
	fw = fopen("dvbiass2_means.txt","w");
	fw2 = fopen("dvbiass2_all.txt","w");
	
	
	for(i=0;i<_greg0.nxy_number;i++){
		/// writes results to files
        for(k=0;k<128;k++)
		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw2,"%d %d %d %d %d %d %d\n",i,_r0[i].sfp,_r0[i].exp,_r0[i].nxy,k,j,nxyresult_at_ch(j,i,k));
				}
			}

		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw,"%d %d %d %d %d %lf\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy,j,nxyresult_at_val(j,i));
				}
			}
			
	}//nxy loop
	fclose(fw);				
	fclose(fw2);
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);
	close_daemon();
	
	free(_r);
	_r=NULL;
	nxyresult_free();
	
	return i;
}

//////////////////////// Vbias scan both functions //////////////////////////
int nxyjob_vbiasboth_init(){
	int i;
	int max;
	
	max=0x1ff;
	
	// if supplied parameter is more than 255, lets assume its upper limit of scan
	if(nxyjob_parameter>0xff){
		max = nxyjob_parameter;
	}
	else{ // otherwise take 512 as default value
		nxyjob_parameter = 0x1ff;
	}
	
	nxyresult_allocate(max+1);
	
	#ifdef DEBUG
	printm("VBiaSboth scan initializing \n");
	printm("scanning up to value %d \n",max);
	#endif
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		#ifdef NXYTER_V2
		_r[i].ext_dacs[2]=1;
		_r[i].ext_dacs[3]=1;
		#else
		_r[i].bias[5]=1;
		#endif
							
		_r[i].config[0]=(_r[i].config[0]&0xf0) + 8; //switch to test trigger mode
	}
	_greg.mode = 1;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);	
	close_daemon();	
	return i;
} 

int nxyjob_vbiasboth_main(){
	int i,j;
	int c;
	int max;
	max = nxyjob_parameter;

	#ifdef NXYTER_V2
	c = _r[0].ext_dacs[2];
	#else
	c=_r[0].bias[5];
	#endif
	
	#ifdef DEBUG
	printm("VBiaS both scan: scanning VbiasS = %d",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			nxyresult_at_ch(c,i,j) = (int)nxyhits_median(&nxydata.phits[i],j);
			}
		nxyresult_at_val(c,i) = nxyint_mean(nxyresult_at(c,i).data);
		
		#ifdef DEBUG
		printm("nxy%d = %lf\n",i,nxyresult_at_val(c,i));
		#endif
		
		#ifdef NXYTER_V2
		_r[i].ext_dacs[2]+=1;
		_r[i].ext_dacs[3]+=1;
		#else
		_r[i].bias[5]++;					
		#endif
	}
	
	#ifdef NXYTER_V2
	if(c>=max){ //check if range is reached (1023 for ext. dac)
		return 1;
	}
	#else
	if(c>=255){ //check if range is reached
		return 1;
	}
	#endif
	
	connect_daemon(nxy_socket_name);
	#ifdef NXYTER_V2
	i = update_daemon(_r,_greg,NXY_EXT_DACS);
	#else
	i = update_daemon(_r,_greg,NXY_BIAS);
	#endif
	

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)
			#ifdef NXYTER_V2
			_r[i].ext_dacs[2]--;
			_r[i].ext_dacs[3]--;
			#else
			_r[i].bias[5]--;
			#endif
	}	
	close_daemon();	
	return 0;
}

int nxyjob_vbiasboth_done(){
	int i,j,k,c=0;
	FILE *fw,*fw2;
	int max;
	double val;
	max= nxyjob_parameter;
	
	#ifdef DEBUG
	printm("VBiaSboth scan finished.\n");
	#endif
	
	fw = fopen("dvbias_means.txt","w");
	fw2 = fopen("dvbias_all.txt","w");
	
	
	for(i=0;i<_greg0.nxy_number;i++){
		int dif = 0;
		int pol = 0;
	
		if((_r0[i].config[1]&4)==4)pol=1; //check polarity setting
		#ifdef DEBUG
		if(pol==1)printm("configuring for positive polarity\n");
		else printm("configuring for negative polarity\n");
		#endif
		
		/// scan through values and check where its start to increase
		for(j=max-1;j>0;j--){
			if(pol==1)k = max-j;
			else k=j;
			
			val = nxyresult_at_val(k,i);
			
			if(nxyresult_at_val(k,i)!=0 && nxyresult_at_val(k+1,i)!=0){
				dif = nxyresult_at_val(k,i)-nxyresult_at_val(k+1,i);
				if(pol==1)dif=-dif; //to have the same difference sign as negative polarity
				}
			else dif=0;
			
			if(dif<-5){
				j--;
				if(pol==1)k = max-j;
				else k=j;
				dif = nxyresult_at_val(k,i)-nxyresult_at_val(k+1,i);
				if(pol==1)dif=-dif; 
				if(dif<-5){c=k;break;} ///2nd difference bigger than 5, lets take the value
				}
		}
		#ifdef DEBUG
		printm("VbiasS: nxy%d -> value = %d\n",i,c);
		#endif
		#ifdef NXYTER_V2
		_r0[i].ext_dacs[2]=c;
		_r0[i].ext_dacs[3]=c;
		#else
		_r0[i].bias[5]=c;
		#endif

		/// writes results to files
        for(k=0;k<128;k++)
		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw2,"%d %d %d %d %d %d %d\n",i,_r0[i].sfp,_r0[i].exp,_r0[i].nxy,k,j,nxyresult_at_ch(j,i,k));
				}
			}

		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw,"%d %d %d %d %d %lf\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy,j,nxyresult_at_val(j,i));
				}
			}
			
	}//nxy loop
	fclose(fw);				
	fclose(fw2);
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);
	close_daemon();
	
	free(_r);
	_r=NULL;
	nxyresult_free();
	
	return i;
}


//////////////////////// Vbias scan at fixed value of other register //////////////////////////
int nxyjob_vbiass_at_init(){
	int i;
	int regval,max;
	int regindex = 3;
	max=0x1ff;
	
	
	regval = nxyjob_parameter;
	
	nxyresult_allocate(max+1);
	
	#ifdef DEBUG
	printm("VBiaSS scan initializing \n");
	printm("scanning up to value %d, fixed at %d \n",max,regval);
	#endif
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].ext_dacs[2]=100; // starting value
		_r[i].ext_dacs[regindex]=regval;
		_r[i].config[0]=(_r[i].config[0]&0xf0) + 8; //switch to test trigger mode
	}
	_greg.mode = 1;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);	
	close_daemon();	
	return i;
} 

int nxyjob_vbiass_at_main(){
	int i,j;
	int c;
	int max;
	max = 0x1ff; //full range
	max = 300;

	
	c = _r[0].ext_dacs[2];
	
	#ifdef DEBUG
	printm("VBiaS both scan: scanning VbiasS = %d",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			nxyresult_at_ch(c,i,j) = (int)nxyhits_median(&nxydata.phits[i],j);
			}
		nxyresult_at_val(c,i) = nxyint_mean(nxyresult_at(c,i).data);
		
		#ifdef DEBUG
		printm("nxy%d = %lf\n",i,nxyresult_at_val(c,i));
		#endif
		
		_r[i].ext_dacs[2]+=1;
	}
	
	if(c>=max){ //check if range is reached (1023 for ext. dac)
		return 1;
	}
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_EXT_DACS);
	

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)
			_r[i].ext_dacs[2]--;
	}	
	close_daemon();	
	return 0;
}

int nxyjob_vbiass_at_done(){
	int i,j,k,c=0;
	FILE *fw,*fw2;
	int max;
	double val;
	int regval;
	char fname[80];
	time_t rawtime;
	struct tm * timeinfo;
	time_t curtime;
	time(&curtime);
	timeinfo = localtime (&curtime);
		
	max= 0x1ff;
	regval = nxyjob_parameter;
	
	#ifdef DEBUG
	printm("VBiaSboth scan finished.\n");
	#endif
	
	time(&rawtime);
	//sprintf(fname,"dvbias_at%d_means.txt",regval);
	strftime (fname,80,"dvbias_%y_%m_%e_%R.txt",timeinfo);
	fw = fopen(fname,"w");
	//sprintf(fname,"dvbias_at%s_all.txt",ctime(&rawtime));
	strftime (fname,80,"dvbias_all_%y_%m_%e_%R.txt",timeinfo);
	fw2 = fopen(fname,"w");
	
	fprintf(fw2,"# scan done at VbiasS2 = %d\n",regval);
	for(i=0;i<_greg0.nxy_number;i++){
		/// writes results to files
        for(k=0;k<128;k++)
		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw2,"%d %d %d %d %d %d %d\n",i,_r0[i].sfp,_r0[i].exp,_r0[i].nxy,k,j,nxyresult_at_ch(j,i,k));
				}
			}

		for(j=0;j<max;j++){
			if(nxyresult_at_val(j,i)){
				fprintf(fw,"%d %d %d %d %d %lf\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy,j,nxyresult_at_val(j,i));
				}
			}
			
	}//nxy loop
	fclose(fw);				
	fclose(fw2);
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_EXT_DACS);
	close_daemon();
	
	free(_r);
	_r=NULL;
	nxyresult_free();
	
	return i;
}

//////////////////////////// Vth scan functions ////////////////////////////////////
int nxyjob_vth_init(){
	int i;
	int max;
	// if supplied parameter is more than 9, lets assume its upper limit of scan
	if(nxyjob_parameter > 9){
		max = nxyjob_parameter;
	}
	else{ // otherwise take 255 as default value
		nxyjob_parameter = 0xff;
		max = 0xff;
	}
	nxydata_status = 0;
	#ifdef DEBUG
	printm("Vth scan initializing \n");
	#endif
	nxyresult_allocate(max+1);
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].bias[2]=1;					
		_r[i].config[0]=0;
	}
	
	_greg.mode = 0;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);	
	close_daemon();	

	return i;
} 



int nxyjob_vth_main(){
	int i;
	int j;
	int c;
	int max;
	c=_r[0].bias[2];
	
	max = nxyjob_parameter;

	#ifdef DEBUG
	printm("Vth scan: scanning Vth = %d .\n",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			nxyresult_at_ch(c,i,j) = nxydata.phits[i].num[j];
		}
		//nxyresult_at_val(c,i) = nxyint_mean(nxyresult_at(c,i).data);
		nxyresult_at_val(c,i) = nxyint_median(nxyresult_at(c,i).data);
		#ifdef DEBUG
			printm("Vth scan: nxy%d av. hits = %d\n",i,(int)nxyresult_at_val(c,i));
		#endif
		_r[i].bias[2]++;
	}

	// check condition to stop the scan
	j=0;
	for(i=0;i<_greg0.nxy_number && c>15;i++){
		j+=nxyresult_at_val(c,i);
		j+=nxyresult_at_val(c-1,i);
	}
	if(j==0 && c>15){ // we have only zeros
		return 1;
	}
	if(c>=max){ // max reached
		return 1;
	}
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_BIAS);


	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)	
			_r[i].bias[2]--;					
	}	
	close_daemon();	

	return 0;
}

int nxyjob_vth_done(){
	int i,j,k;
	int max;
	char buf[20];
	FILE *fw;
	fw = fopen("dvth_all.txt","w");
	#ifdef DEBUG
	printm("VTh scan finished.\n");
	#endif
	max = nxyjob_parameter;
	
	// write results
	for(i=0;i<_greg0.nxy_number;i++){
		for(j=1;j<128;j++){ // nxy channel loop
			for(k=0;k<max;k++){ //vth reg loop
				fprintf(fw,"%d %d %d %d %d %d %d\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy, j,k, nxyresult_at_ch(k,i,j));			
				if(k>15 && nxyresult_at_val(k,i)==0){
					break;
					}
				}
			}
			_r0[i].bias[2]=k;
		
		#ifdef DEBUG
		printm("Vth: nxy%d -> value = %d\n",i,k);
		#endif
	}//nxy loop
	
	fclose(fw);	
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);
	close_daemon();
	
	nxyresult_free();
	free(_r);
	_r=NULL;
	return i;
}


//////////////////////////// local thresholds scan ////////////////////////////////////
int nxyjob_lth_init(){
	int i,j;
	
	#ifdef DEBUG
	printm("Localth. scan initializing \n");
	#endif
	
	// allocate for 32 scans
	nxyresult_allocate(32);	
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		if(nxyjob_parameter<255 && nxyjob_parameter>5){
			 _r[i].bias[2]=nxyjob_parameter; 
			}
		#ifdef DEBUG
		printf("nxyter %d, Vth = %d\n",i,_r[i].bias[2]);
		#endif
		_r[i].config[0]=0; // switch off all test modes in case are enabled
		for (j = 0; j < 128; j++)
		{
			_r[i].thr[j]=1; // starting value for local thresholds
		}
		
	}
	
	_greg.mode = 0;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_TH);	
	close_daemon();	

	return i;
} 

int nxyjob_lth_main(){
	int i;
	int j;
	int c;

	c=_r[0].thr[0];
	#ifdef DEBUG
    printm("lth scan: scanning lth = %d\n",c);
    #endif

	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			nxyresult_at_ch(c,i,j) = nxydata.phits[i].num[j];
			_r[i].thr[j]++;
			}
		nxyresult_at_val(c,i)=nxyhits_nhitmean(&nxydata.phits[i]);
		}
	
	if(c>30){ 
		return 1;
	}
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_TH);
	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)
			for(j=0;j<128;j++)
				_r[i].thr[j]++;					
		}
	close_daemon();
	return 0;
}

int nxyjob_lth_done(){
	int i,j,k,c;
	char buf[20];
	int d[32];
	double dd;
	FILE *fw;
	fw = fopen("dlth_all.txt","w");

	#ifdef DEBUG
	printm("Local Th scan finished.\n");
	#endif
	
	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			for(k=1;k<32;k++){
				fprintf(fw,"%d %d %d %d %d %d %d\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy,j,k,nxyresult_at_ch(k,i,j));
			}
		}
	}//nxy loop	
	nxyresult_free();
	fclose(fw);			
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_TH);
	close_daemon();
	return i;
}


//////////////////////////// local thresholds equalization ////////////////////////////////////
int nxyjob_ltheq_init(){
	int i,j;
	
	#ifdef DEBUG
	printm("Localth. scan initializing \n");
	#endif
	
	// allocate for 32 scans
	nxyresult_allocate(40);	
	
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	nxydata_status = 0;
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].bias[2]=40; 
		_r[i].config[0]=0; // switch off all test modes in case are enabled
		for (j = 0; j < 128; j++){
			_r[i].thr[j]=17; // starting value for local thresholds, 
							 // some middle value is used 1st as before
							 // local th. scan we try to fix Vth 
		}
		
	}
	
	_greg.mode = 0;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_TH);	
	close_daemon();	

	return i;
} 

int nxyjob_ltheq_main(){
	int i;
	int j;
	int c,c2;

	c=_r[0].thr[0];
	c2 = _r[0].bias[2];

	// searching for Vth setting at which localthresholds will be adjusted
	if(nxydata_status==0){
		#ifdef DEBUG
			printm("localth eq: scanning for Vth, Vth = %d\n",c2);
		#endif
		for(i=0;i<_greg0.nxy_number;i++){
			  nxyresult_at_val(c2,i) = nxyint_mean(&nxydata.phits[i].num);
			   //nxyresult_at_val(c2,i) = nxyint_median(&nxydata.phits[i].num);
			#ifdef DEBUG
				printm("nxy%d av. hits = %d\n",i,(int)nxyresult_at_val(c2,i));
			#endif
			_r[i].bias[2]--;
			}
		if(c2<=6){
			#ifdef DEBUG
				printm("localth eq: Vth scan finish, local thr. will be scanned at:",c2);
			#endif
			for(i=0;i<_greg0.nxy_number;i++){
				//double val;
				//_r[i].bias[2] = nxyresult_maxindex(40,i);
				//val = nxyresult_at_val(_r[i].bias[2],i);
				_r[i].bias[2] = nxyresult_closestindex(1000,40,i);
				#ifdef DEBUG
					printm("nxy%d Vth = %d",i,_r[i].bias[2]);
				#endif
				// now for the next step set local threshold to initial value
				for (j = 0; j < 128; j++){
					_r[i].thr[j]=1; // starting value for local thresholds
					}
				}
			nxydata_status = 1;
		}
		// now Vth initial scan is finished
		connect_daemon(nxy_socket_name);
		if(update_daemon(_r,_greg,NXY_BIAS)!=1){
			for(i=0;i<_greg0.nxy_number;i++){
				_r[i].bias[2]++;
				}
			}
		return 0;
		} //end of initial Vth scan, nxydata_status == 0
		
	// we have Vth, now scan the local thresholds
	else{
		#ifdef DEBUG
		printm("lth eq: scanning lth = %d\n",c);
		#endif
		for(i=0;i<_greg0.nxy_number;i++){
			for(j=0;j<128;j++){
				nxyresult_at_ch(c,i,j) = nxydata.phits[i].num[j];
				_r[i].thr[j]++;
				}
				nxyresult_at_val(c,i) = nxyresult_at_val(_r[i].bias[2],i); //this is to copy mean number of hits at fixed Vth value for later reference
			}
		connect_daemon(nxy_socket_name);
		i = update_daemon(_r,_greg,NXY_TH);
		if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)
			for(j=0;j<128;j++)
				_r[i].thr[j]--;					
		}
		
		} // end of local threshold scan, nxydata_status == 1

	if(c>30){ 
		return 1;
	}
		
	close_daemon();
	return 0;
}

int nxyjob_ltheq_done(){
	int i,j,k,c,c2;
	int d[32];
	int index;
	FILE *fw;
	fw = fopen("dlth_all.txt","w");

	#ifdef DEBUG
	printm("Local Th scan finished.\n");
	#endif
	
	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			memset(d,0,sizeof(d));
			for(k=1;k<31;k++){
				fprintf(fw,"%d %d %d %d %d %d %d\n",i, _r0[i].sfp,_r0[i].exp,_r0[i].nxy,j,k,nxyresult_at_ch(k,i,j));
				if(k>3 && k<29) // avoid edge of the range
					d[k] = nxyresult_at_ch(k,i,j);
			}
			index = nxyint_closestindex(d,nxyresult_at_val(31,i),31);
			if(index == 0)index = 17; //something went wrong, set some default value
			_r0[i].thr[j] = index; // update local thresholds
			printf("ch%d, aim = %d, th = %d, val =%d\n",j,(int)nxyresult_at_val(31,i),_r0[i].thr[j],nxyresult_at_ch(_r0[i].thr[j],i,j));
		} // end of channel loop
		
	}//nxy loop	
	
	#ifdef DEBUG
	for(i=0;i<_greg0.nxy_number;i++){
		printf("NXYTER %d\n",i);
		for(j=0;j<8;j++){
			for(k=0;k<16;k++){
			printf("%d ",_r0[i].thr[ (16*j)+k]);
			}
			printf("\n");
		}
	}
	#endif
	nxyresult_free();
	fclose(fw);			
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_TH);
	close_daemon();
	return i;
}

///////////////// Baseline reader jobs ////////////////////////////////////////////
int nxyjob_baselines_init(){
	int i;
	
	#ifdef DEBUG
	printm("Baseline read initializing \n");
	#endif
	nxydata_job_stage=1;

	nxyresult_allocate(1);

	connect_daemon(nxy_socket_name);
	read_daemon(&_r0,&_greg0);
	close_daemon();
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].config[0]=(_r[i].config[0]&0xf0) + 8; //switch to test trigger mode
		
	}

	_greg.mode = 1;

	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_RECEIVER|NXY_CONFIG);	
	close_daemon();	
	return i;
} 

int nxyjob_baselines_main(){
	int i,j;

	#ifdef DEBUG
	printm("Baseline read\n");
	#endif
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			int v;
			nxyresult_at_ch(0,i,j)=(int)nxyhits_median(&nxydata.phits[i],j);
			}
	}	
	return 1;
}

int nxyjob_baselines_done(){
	int i,j;
	int val;
	FILE *fw;
	
	#ifdef DEBUG
	printm("Saving Baselines to the file\n");
	#endif
	
	if(nxyjob_parameter==1){
		char fname[80];
		struct tm * timeinfo;
		time_t curtime;
		time(&curtime);
		timeinfo = localtime (&curtime);
		strftime (fname,80,"baselines_%y_%m_%e_%R.txt",timeinfo);
		fw = fopen(fname,"w");
	}
	else{
		fw = fopen("baselines.txt","w");
	}
	
	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			val  = nxyresult_at_ch(0,i,j);
			fprintf(fw,"%d %d %d %d %d\n",_r0[i].sfp, _r0[i].exp, _r0[i].nxy,j,val);
		}
	}

	free(_r);
	_r=NULL;
	nxyresult_free();
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG | NXY_RECEIVER);			
	close_daemon();	
	fclose(fw);
	return i;
}
//////////////////////////////////////////////////

//////////// Internal Calibration Jobs ///////////
int nxyjob_internal_calibration_init(){
        int i;
                
        #ifdef DEBUG
        printm("Internal Calibration initializing \n");
        #endif
        
        nxyresult_allocate(255);
        nxydata_job_stage=1;
        nxyregcpy(&_r,&_r0,&_greg,&_greg0);
        if(_r==NULL){printf("not allocated\n");exit(1);}
        
        for(i=0;i<_greg0.nxy_number;i++)	
        {
                _r[i].bias[8]=1; // int. calibration register
                _r[i].config[0]=1; //test pulse mode
                _r[i].config[1]=_r[i].config[1]&0xfc; //set calibration bits to 00
        }
        _greg.mode = 1;
        connect_daemon(nxy_socket_name);
        i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);	
        close_daemon();	
        return i;        
}

int nxyjob_internal_calibration_main(){
        int i,j,k;
        int c,group;

        c=_r[0].bias[8];
        group = _r[0].config[1]&0x3;

	#ifdef DEBUG
	printm("Internal Calibration scan: scanning cal = %d, group = %d.\n",c,group);
	#endif

	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=group;j<128;j+=4){
			nxyresult_at_ch(c,i,j) = (int)nxyhits_median(&nxydata.phits[i],j);
			}
		//nxyresult_at_val(c,i) = nxyint_mean(nxydata_results[i].data[c]); 
		
        //switch to next calibration group
        if(group<3){
            _r[i].config[1]++;
            }
        //all group done, go to next cal value 
        if(group==3){
            _r[i].bias[8]+=nxyjob_parameter;
            _r[i].config[1]=_r[i].config[1]&0xfc;
            }
	} // nxy loop
	
	if( (c+nxyjob_parameter >255) && (group==3)){
		return 1;
	}
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_BIAS);

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)	
			_r[i].bias[8]-=nxyjob_parameter;					
	}	
	close_daemon();	
	return 0;
}

int nxyjob_internal_calibration_done(){
	int i,j,k;
	char buf[20];
	FILE *fw;
	fw = fopen("dintcal.txt","w");
	
	#ifdef DEBUG
	printm("Internal calibration finished.\n");
	#endif
	
	for(i=0;i<_greg0.nxy_number;i++){
                for(k=0;k<128;k++){ //nxy channel loop
                        for(j=1;j<255;j+=nxyjob_parameter){ // int cal reg value loop
				fprintf(fw,"%d %d %d %d\n",i, k,j,nxyresult_at_ch(j,i,k));
				}
			}
	}//nxy loop

	fclose(fw);	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);
	close_daemon();
	free(_r);
	_r=NULL;
	nxyresult_free();
	return i;
}

///////////////// Baseline reader jobs ////////////////////////////////////////////
int nxyjob_save_config_done(){
	char fname[30];
	#ifdef DEBUG
	printm("Saving GMX config\n");
	#endif
	struct tm * timeinfo;
	time_t curtime;
	time(&curtime);
	timeinfo = localtime (&curtime);
	strftime (fname,80,"nxy_%y_%m_%e_%R.txt",timeinfo);

	connect_daemon(nxy_socket_name);
	read_daemon(&_r0,&_greg0);
	close_daemon();
	
	save_nxy_file(fname,_greg0,_r0);
	
	return 1;
} 

//////////////////////////////////////////////////////////////////

void nxydata_fill(int _sfp,int _exp, int _nxy, int _ch, int _adc){
	int index;
	if(_ch<0 || _ch>128)return;
	if(_sfp<0 || _sfp>MAX_SFP)return;
	if(_exp<0 || _exp>MAX_EXP)return;
	if(_nxy<0 || _nxy>2)return;
	if(_adc<1 || _adc>4095)return;
	
	index = nxy_map(_sfp,_exp,_nxy,_greg0);
        if(index<0)return;
	if(nxydata.phits[index].num[_ch]<MAXSTAT){
                nxydata.phits[index].hits[_ch][nxydata.phits[index].num[_ch]] = _adc;
        }
	nxydata.phits[index].num[_ch]++;
}
//////////////////////////////////////////////////////////////
//////////////// nxydata_unpack ///////////////////////
int nxydata_unpack(long *pdata){
	unsigned int first,second;
	int nxy_n_cha;
	int i,j,k,l;
	int ch, adc;

	if ( (*pdata)==0xbad00bad )
  	{
    		printm ("found bad event \n");
	    	return 0;
  	}

	for(i=0;i<MAX_SFP;i++){
		if(_greg0.sfp_in_use[i][0]<1)continue;
		
		//skip padding
		for(j=0;j<100;j++){
			first = *pdata++;
			if( (first&0xfff00000) != 0xadd00000){
				pdata--;
				break;
				}
			}
		for(j=0;j<MAX_EXP;j++){
			if(_greg0.sfp_in_use[i][j]==0)break;
		
			for(k=0;k<_greg0.sfp_in_use[i][j];k++){
				///exploder header
				first = *pdata++;
				if( ((first & 0xff) >> 0) != 0x34 ){
            				printm ("ERROR>> channel header type is not 0x34 \n");
		            		return 0;
          			}
				if( ((first & 0xff0000) >> 16) != j ){
            				printm ("ERROR>> exploder id is wrong \n");
		            		return 0;
	          		}
		    		if( ((first & 0xff000000) >> 24) != k )
				{
            				printm ("ERROR>> Nxyter id is wrong \n");
		            		return 0;
	          		}

				///data size
				first = *pdata++;
				nxy_n_cha = (first - 20) >> 3;
				
				///nxy header
				first = *pdata++;
				if( ((first & 0xff000000) >> 24) != 0xaa)
				{
		            		printm ("ERROR>> Nxyter header id is not 0xaa \n");
		         		return 0;
          			}

				////////////////////////////////////////////////////////
				/// trigger counter from nxy header can be read here ///
				////////////////////////////////////////////////////////
				
				first = *pdata++;
				second = *pdata++;

				////////////////////////////////////////////////////////
				/// epoch time should be read here ///
				////////////////////////////////////////////////////////
				
				for(l=0;l<nxy_n_cha;l++){
					first = *pdata++;
					second = *pdata++;

					ch = (second & 0x7f000000) >> 24;
					adc = (first & 0xfff0000) >> 16;
					nxydata_fill(i,j,k,ch,adc);
				}///end of channels loop

				///error pattern
				first = *pdata++;
				if( ((first & 0xff000000) >> 24) != 0xee)
				{
					printm ("ERROR>> Nxyter error pattern is not 0xee \n");
					return -1;
          			}
          		
	          		///nxy trailer
	          		first = *pdata++;
	          		if( ((first & 0xff000000) >> 24) != 0xbb)
				{
		            		printm ("ERROR>> Nxyter trailer id is not 0xbb \n");
		            		return -1;
	          		}
				
			} ///loop over NXY
		}/// loop over EXP

	} //end SFP loop

	nxydata_event_counter++;
	if(nxydata_event_counter>=MAXSTAT)nxydata_ready = 1;
	
	return nxydata_event_counter;
}

int compare(const void *a, const void *b)
{
	return (*(int*)a -*(int*)b);
	}

double nxyhits_mean(struct nxyhits *n, int ch){
    int sum=0;
    int i;

    if(n->num[ch]==0)return -1;
    if(ch>128 || ch<0)return -1;
    for(i=0;i<n->num[ch];i++)
    {
        sum+=n->hits[ch][i];
    }

    return (double)sum/n->num[ch];
}

double nxyhits_median(struct nxyhits *n, int ch){
	int e;
	if(ch>128 || ch<0)return -1;
    if(n->num[ch]==0)return 0;
    if(n->num[ch]==1)return (double)n->hits[ch][0];
    if(n->num[ch]>MAXSTAT)
		e = MAXSTAT-1;
    else
		e=n->num[ch]-1;
    qsort(n->hits[ch],e,sizeof(int),compare);
	if(e%2==0){
		return (double)0.5*(n->hits[ch][(e/2)] + n->hits[ch][(e/2)-1]);
	}
	else
		return (double) n->hits[ch][e/2];
}


double nxyhits_nhitmean(struct nxyhits *n){
    int sum=0;
    int i;

    for(i=0;i<128;i++)
    {
        sum+=n->num[i];
    }

    return (double)sum/128.0;
}

double nxyint_median(int *b){
	qsort(b,128,sizeof(int),compare);
	return (double)0.5*(b[64] + b[63]);
}

double nxyint_mean(int *b){
    int sum = 0;
    int i=0,n=0;

    for(i=0;i<128;i++){
        sum+=b[i];
        if(b[i]>0)n++;
    }
    if(n==0)return 0;
    return (double)sum/128.0;
}

double nxyint_rms(int *b, int num){
	int i;
	double sum = 0;
	int mean = 0;
	int count=0;

	for(i=0;i<num;i++){
	        if(b[i]>0){
			sum+=b[i];
			count++;
			}
	    }
	if(count==0)return 99999999;
	//if(count<64)return 99999999;
	mean = sum/count;

	sum=0;count=0;
	for(i=0;i<num;i++){
		//if(b[i]>0){
		sum+=(b[i]-mean)*(b[i]-mean);
		//count++;
		//}
	}
	sum = sum/num;
	sum = sqrt(sum);
	return sum;
}

int nxyint_maxindex(int *b, int num){
	int i,index;
	int max;

	max = b[0];
	index = 0;
	for(i=1;i<num;i++){
		if(b[i]>max)
			{
			max = b[i];
			index = i;
			}
		}
	return index;
}

int nxyresult_maxindex(int num, int nxy){
	int i,index;
	int max;
	index = 0;
	max = nxyresult_at_val(0,nxy);
	for(i=1;i<num;i++){
		if(nxyresult_at_val(i,nxy)>max){
			max = nxyresult_at_val(i,nxy);
			index = i;
		}
	}
	return index;
}


int nxyresult_closestindex(double aim, int num, int nxy){
	int index;
	double dif;
	int i;

	index = 0;
	dif = abs(nxyresult_at_val(0,nxy)-aim);
	for(i=1;i<num;i++){
		if(abs(nxyresult_at_val(i,nxy)-aim)<dif){
			dif = abs(nxyresult_at_val(i,nxy)-aim);
			index = i;
		}
	}
	return index;
}

int nxyint_closestindex(int *b, int aim, int num){
        int index;
        int dif;
        int i;

        index = 0;
        dif = abs(b[0]-aim);
        for(i=1;i<num;i++){
                if(abs(b[i]-aim)<dif){
                        dif = abs(b[i]-aim);
                        index = i;
                }
        }

        return index;
}


