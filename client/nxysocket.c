/**
 * @file 
 * A. Prochazka
 * 11.1.2016
 * functions for connecting to nyxor daemon
 */
#include "nxysocket.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys/socket.h"
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>

#ifdef NXYDAEMON_BUILD
#ifndef NXYDAEMON_NOHTTP
#include "mongoose.h"
#include "mongoose.c"
#endif
#include "time.h"
#endif
///////////////////  Socket helping function //////////////////////////

#ifdef NXYDAEMON_BUILD
int nxy_connection_accept(struct nxy_connection_struct *con){
    struct sockaddr_storage client;
    socklen_t client_len=sizeof(client);
    int length;
    int status;
    
    con->client_socket_fd = accept(nxyd_fd,(struct sockaddr *)&client, &client_len);
    if(con->client_socket_fd<0){printf("could not accept connection\n");return -1;}
    

    time_t curtime;
    curtime = time(NULL);
    printf("Accepted: %s\n",ctime(&curtime));
    
    status = unpackdata2(con->client_socket_fd,&length,&con->com);
    if(status<1){printf("data corrupted (%d)\n",status);return -1;}
    con->protocol_type = status;
    con->reply_type = con->protocol_type;
    
    return 1;
}

int nxy_connection_init(struct nxy_connection_struct *con, struct nxy_reply_struct *reply){
    con->nxyreply = reply;
}

int nxy_connection_close(struct nxy_connection_struct *con){
    if(close(con->client_socket_fd)!=0){
        printf("Error while closing the network connection! \n");
    }
}


int nxy_connection_send(struct nxy_connection_struct *con){
    int status;
    status = sendall(con->client_socket_fd,con->nxyreply->msg,&con->nxyreply->len);
    return status;
}

int nxy_connection_error(struct nxy_connection_struct *con){
    int status;
    if(con->reply_type == NXYD_PROTOCOL){
        strcpy(con->nxyreply->msg,"BAD");
        con->nxyreply->len = 3;
        status = sendall(con->client_socket_fd,con->nxyreply->msg,&con->nxyreply->len);
        if(status==-1)printf("sending error\n");
        return status;
        }
    
    else{ // other is just HTTP protocol atm
        strcpy(con->nxyreply->msg,"HTTP/1.1 400\r\nContent-Length:0\r\n\r\n");
        con->nxyreply->len = strlen(con->nxyreply->msg);
        status = sendall(con->client_socket_fd,con->nxyreply->msg,&con->nxyreply->len);
        return status;
        }
        
    return 1;
}

int nxy_connection_OK(struct nxy_connection_struct *con){
    int status;
    if(con->reply_type == NXYD_PROTOCOL){
        strcpy(con->nxyreply->msg,"AOK");
        con->nxyreply->len = 3;
        status = sendall(con->client_socket_fd,con->nxyreply->msg,&con->nxyreply->len);
        return status;
        }
    
    return 1;
}
#endif

void nxy_reply_msg_clear(){
        memset(nxy_reply.msg,0,nxy_reply.size);
}

void* nxy_reply_msg_init(int nxynum){
        int len=0;
        len = sizeof(struct globalreg);
        len+= nxynum*sizeof(struct nxyreg);
        len+=2000*nxynum; // just guessed margin for txt size
        nxy_reply.msg = (char *)malloc(len);
        nxy_reply.size = len;
        nxy_reply.len = 0;
        return nxy_reply.msg;
}

int sendall(int s, char *buf, int *len)
{
    int total = 0;        // how many bytes we've sent
    int bytesleft = *len; // how many we have left to send
    int n;

    while(total < *len) {
        n = send(s, buf+total, bytesleft, 0);
        if (n == -1) { break; }
        total += n;
        bytesleft -= n;
    }

    *len = total; // return number actually sent here

    return n==-1?-1:1; // return -1 on failure, 1 on success
}

int recvall(int s, char *buf, int len){
    int total = 0;

    int n = 0;

    while(total<len){
        n = recv(s,buf,len-total,0);
        if(n<1){return n;break;}
        total+=n;
        buf+=n;
    }
//    if(total!=len)total = -1;
    return total;
    }

/// function to pack the data stream into header and trailers and prepare it before sending
char* pack(int com, int *length, char *data)
{
    int len;
    int dummy;
    char *buf = NULL;
    char *save=NULL;
    if(data==NULL)*length=0;
    len = *length+4*(sizeof(int)); //data + header + size + command + tail

    //alocation of memory for sending
    buf = (char*)malloc(len);
    if(buf==NULL){return NULL;}

    //header
    dummy = 0xab00;
    dummy+=sizeof(struct nxyreg) + sizeof(struct globalreg);
    save = buf;

    memcpy(save,&dummy,sizeof(int));
    save+=sizeof(int);

    //data size
    memcpy(save,length,sizeof(int));
    save+=sizeof(int);

    //command
    memcpy(save,&com,sizeof(int));
    save+=sizeof(int);

    //data itself
    if(*length>0){
        memcpy(save,data,*length);
        save+=*length;
    }

    //tail
    dummy=0xe;
    memcpy(save,&dummy,sizeof(int));
    save+=sizeof(int);

    if((save-buf)!=len){free(buf);buf=NULL;return NULL;}
    *length = len;
    return buf;
}

/// function to read data stream from fd file descriptor (network socket) and check headers and trailers
char* unpackdata(int fd,int *len,int *com){
    int i[3];
    int length;
    char *buf=NULL;
    *com = 0;
    length = recv(fd,i,3*sizeof(int),0);
	if(length!=(3*sizeof(int))){return NULL;}
	if(i[0]!=(0xab00+sizeof(struct nxyreg) + sizeof(struct globalreg))){return NULL;} //header check
	if(i[1]<0){return NULL;}
    *len = i[1]+sizeof(int);

    buf = (char*)malloc(i[1]+sizeof(int)); //data size + tail
    memset(buf,0,i[1]);
    length = recvall(fd,buf,i[1]+sizeof(int));
    if(length!=*len)return NULL;
    *com = i[2];
    if(*(buf+i[1])!=0xe)return NULL;
    return buf;
}

#ifdef NXYDAEMON_BUILD
/// just a test
int unpackdata2(int fd,int *len,int *com){
    int *pint=NULL;
    int length;
    int totallength;
    char *buf=NULL;
    *com = 0;
    #ifndef NXYDAEMON_NOHTTP
    struct http_message httpmessage;
    #endif
    int status;
    
    //length = recvall(fd,nxy_reply.msg,nxy_reply.size);
    length = recv(fd,nxy_reply.msg,nxy_reply.size,0);
    nxy_reply.len = length;
    pint = (int *)nxy_reply.msg;
    
    #ifdef DEBUG
    printf("received %d bytes \n",length);
    printf("first word: 0x%x \n",pint[0]);
    #endif
    
    
	if(length<sizeof(int)){return -1;}
    
	if(pint[0]==(0xab00+sizeof(struct nxyreg) + sizeof(struct globalreg))){ //NXYD protocol
        if(pint[1]<0){return -2;}
        *com = pint[2];
          #ifdef DEBUG
        printf("NXYD protocol\n");
        #endif
        *len = pint[1]+1;
        totallength = (3*sizeof(int)) + *len;
        if( totallength >= nxy_reply.size){
            printf("reply buffer too small\n");
            return -3;
        }
        
        if(totallength>length){
            printf("not all received, waiting for other packets\n");
            status = recvall(fd,nxy_reply.msg+length,(totallength-length));
            if(status!=(totallength - length))return -4;
        }
        
        nxy_reply.len=totallength;
        buf = memmove(nxy_reply.msg,&pint[3],*len);
        if(*(buf+*len-1)!=0xe)return -5;
        return 1;
        }//end of nxyd protocol header check
    
    #ifndef NXYDAEMON_NOHTTP
    
    #ifdef DEBUG
    printf("HTTP protocol\n");
    #endif 
    status = mg_parse_http(nxy_reply.msg,length,&httpmessage,1);
    if(status <1)return -6;
    *len = httpmessage.body.len;
    #ifdef DEBUG
    printf("body length = %d\n",*len);
    #endif
    
    if(*len>=nxy_reply.size){
        printf("Data larger than allocated size, ignoring !!!\n");
        return -10;
    }
   
    
    *com='r'<<24; //just default action in case unknown uri is given
    if(mg_vcmp(&httpmessage.uri, "/")==0){
        *com='r'<<24;
    }
    
    if(*len>0){
        buf = strstr(nxy_reply.msg,"\r\n\r\n")+4; //skip and move behind the header
        totallength = buf - nxy_reply.msg + *len; // total length received including the header size
        if(totallength > length){
            #ifdef DEBUG
            printf("receiving rest of the data \n");
            #endif
            status = recvall(fd,nxy_reply.msg+length,(totallength-length));
            if(status!=(totallength - length))return -4;    
        }        
        
        buf = memmove(nxy_reply.msg,buf,*len); // move body to nxy reply structure
        buf[*len]='\0'; // add terminating null character
        //buf = memmove(nxy_reply.msg,httpmessage.body,*len);    
        #ifdef DEBUG
        printf("message =\n%s \n",nxy_reply.msg);
        #endif
    }
    
    return 2;
    #else
    return -1;
    #endif
    
    
}
#endif

/// convert nxyter and exp. configs to data stream
char* nxydata2str(struct globalreg greg, struct nxyreg *r,int *len)
{
    int length=0;
    char *buf=NULL;

    length = sizeof(struct globalreg)+(greg.nxy_number*sizeof(struct nxyreg));

    buf = (char*)malloc(length);
    if(buf==NULL)return NULL;

    memcpy(buf,&greg,sizeof(greg));
    memcpy(buf+sizeof(greg),r,greg.nxy_number*sizeof(struct nxyreg));

    *len = length;
    return buf;
}

/// convert data stream to xnyter and expl. data
struct nxyreg* str2nxydata(char* buf, struct globalreg *greg)
{
    struct nxyreg *r=NULL;
    if(buf == NULL)return NULL;
    memcpy(greg,buf,sizeof(struct globalreg));

    r=(struct nxyreg*)malloc(greg->nxy_number*sizeof(struct nxyreg));
    if(r==NULL)return NULL;
    memcpy(r,buf+sizeof(struct globalreg),greg->nxy_number*sizeof(struct nxyreg));

    return r;
}


int connect_daemon(char *sname){
    struct addrinfo hints, *servinfo,*p;
    int status;
   
    memset(&hints, 0,sizeof(hints));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(sname,NXYD_PORT,&hints,&servinfo);
    if(status != 0)return -1;

        for(p=servinfo;p!=NULL;p=p->ai_next){
        nxyd_fd = socket(p->ai_family,p->ai_socktype,p->ai_protocol);
        if(nxyd_fd<0){freeaddrinfo(servinfo);return -1;}
        status = connect(nxyd_fd,p->ai_addr,p->ai_addrlen);
        if(status<0){freeaddrinfo(servinfo);close(nxyd_fd);return -1;}
        break;
    }
    if(p==NULL){freeaddrinfo(servinfo);close(nxyd_fd);return -1;}
    freeaddrinfo(servinfo);

    return nxyd_fd;
}

int close_daemon(){
    int status;
    int com = 0;
    int len;
    //char message[3];
    char *buf=NULL,*save=NULL;

    com  = (int)'B'<<24;
    //save = nxydata2str(greg,r,&len);
    buf = pack(com,&len,save);
    free(save);
    if(buf==NULL)return -1;
    status = sendall(nxyd_fd,buf,&len);
    free(buf);
    close(nxyd_fd);
    return 1;
}


int read_daemon(struct nxyreg **r, struct globalreg *greg){
    int status;
    int com = 0;
    int len;
    char *buf;

    com  = (int)'R'<<24;

    buf = pack(com,&len,NULL);
    if(buf==NULL)return -2;
    status = sendall(nxyd_fd,buf,&len);
    if(status < 1 ){free(buf);return -2;}
    free(buf);

    buf =unpackdata(nxyd_fd,&len,&com);
    if(buf == NULL){return -1;}
    *r = str2nxydata(buf,greg);
    if(*r==NULL){free(buf);return -1;}
    
    free(buf);
    return 1;
}


int update_daemon(struct nxyreg *r, struct globalreg greg,int mode){
    int status;
    int com = 0;
    int len;
    char message[3];
    char *buf=NULL,*save=NULL;

    com  = (int)'w'<<24;
    com += mode;

    save = nxydata2str(greg,r,&len);
    buf = pack(com,&len,save);
    free(save);
    if(buf==NULL)return -2;
    status = sendall(nxyd_fd,buf,&len);
    if(status < 1 ){free(buf);return -2;}
    free(buf);
    status = recv(nxyd_fd,message,3,0);
    if(memcmp(message,(char *)"AOK",3)!=0)return -1;
    return 1;
}

int reset_daemon(struct nxyreg *rr, struct globalreg gg, int testmode){
    int status;
    int com = 0;
    int len;
    char message[3];
    char *buf=NULL,*save=NULL;


    if(testmode){
        com  = (int)'T'<<24;
    }
    else{
       com  = (int)'W'<<24;
    }
	com += (255<<16); //reset
	com += 255;

    save = nxydata2str(gg,rr,&len);
    buf = pack(com,&len,save);
    free(save);
    if(buf==NULL)return -2;
    status = sendall(nxyd_fd,buf,&len);
    if(status < 1 ){free(buf);return -2;}
    free(buf);
    status = recv(nxyd_fd,message,3,0);
    if(memcmp(message,(char *)"AOK",3)!=0)return -1;
    return 1;
}
