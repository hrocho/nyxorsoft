/**
 * @file
 * A.Prochazka
 * 11.1.2016
 * here are the functions used to manipulate with global and nxyter registers
 * including function for communicating with the nxyter daemon
 * 
 * last changes:
 * - register value type is now defined via #define macro
 * - config file keywords are now defined via #define macro
 * - modified for nxyter v2
 */ 
#ifndef NXYREG_H
#define NXYREG_H
#define DEBUG


#define MAX_SFP 4
#define MAX_EXP 16

#define NXYTER_V2 

//new GEMEX nyxor version
#define IN_USE_KEY "NYX_IN_USE"
#define EXP_KEY "NYX"
#define NXY_KEY "nXY"
// older GEMEX version with exploder
//#define IN_USE_KEY "EXP_IN_USE"
//#define EXP_KEY "EXP"
//#define NXY_KEY "NXY"


#define REG_INT_TYPE int16_t

#include <stdint.h>

/**
 * enumeration type to indicate which register to update 
 * is is user for nxy_write* function from nxyctrl.h
 */
enum nxy_update_t {NXY_MASK=1,NXY_BIAS=2,NXY_CONFIG=4,
					NXY_EXT_DACS=8,
					NXY_TH=16,
					NXY_CLOCK_DELAY=32, 
					NXY_GLOBAL = 64, 
					NXY_RECEIVER = 128, 
					NXY_ALL = 255};

/**
 * structure to store registers values for 1 nxyter, additionally to register it contains 
 * variable do identification of the nxyter, ie sfp, exp and nxy number.
 */
struct nxyreg{
  REG_INT_TYPE i2c_addr;
  REG_INT_TYPE reset;
  REG_INT_TYPE mask[16];
  REG_INT_TYPE bias[14];
  REG_INT_TYPE config[2];
  REG_INT_TYPE te_del[2];
  REG_INT_TYPE clk_del[3];
  REG_INT_TYPE thr_te;
  REG_INT_TYPE thr[128];
  REG_INT_TYPE adc_dco_phase;
  REG_INT_TYPE sfp;
  REG_INT_TYPE exp;
  REG_INT_TYPE nxy;
  #ifdef NXYTER_V2
  REG_INT_TYPE ext_dacs[4]; //external DACs
  #endif
  
};

/**
 * structure to store global register values.
 * additionally it contains data:
 * - about sfp configurations:- sfp_in_use, 
 * - nxy_number - total number of nxyters and mode - working mode
 * - mode = 0 is normal operation mode,
 * - mode = 1 is test mode
 * 
 */ 
struct globalreg{
	int32_t pre_trg_wind;
	int32_t pos_trg_wind;
	int32_t test_pul_del;
	int32_t test_trg_del;
  	REG_INT_TYPE sfp_in_use[MAX_SFP][MAX_EXP];
  	REG_INT_TYPE nxy_number;
	REG_INT_TYPE nxy_ctrl[MAX_SFP][MAX_EXP]; // nxy_ctrl register variable
  	REG_INT_TYPE mode; // exploder mode 0 = normal mode, 1 = test mode
};

/**
 * This function return the nxyter number (index) according to specified sfp, exp and nxy 
 * parameter and supplied global configuration from globalreg structure @see globalreg
 * @return nxy index on success, on fail it returns -1
 */ 
int nxy_map(int sfp, int exp, int nxy, struct globalreg greg);

/**
 * This function return the sfp, exp and nxy number of the nxyter with nxyter index specified by parameter index 
 * and supplied global configuration from globalreg structure @see globalreg
 * parameters *sfp, *exp and *nxy will be updated
 * @return on success return 1, otherwise 0
 */ 
int nxy_id(int index, REG_INT_TYPE *sfp, REG_INT_TYPE *exp, REG_INT_TYPE *nxy, struct globalreg greg);

//int nxyreg_prepare(struct nxyreg *r, struct globalreg greg);

/**
 * Copies the nxyreg structure from r0 to r and globalreg structure from g0 to greg
 * if r need to be allocated *r should poits to NULL and it will be allocated
 * otherwise it is assumed the memory is already allocated with sufficient size
 * @param  **r is the destination nxyreg structure pointer.
 * @param  **r0 is the source from where nxyreg structure
 * @param *g is pointer to the destination globalreg structure
 * @param *g0 is the pointer to the source globareg structure
 * @return pointer to pointer to destination nxyreg structure , on fail NULL is returned
 */ 

void* nxyregcpy(struct nxyreg **r, struct nxyreg **r0, struct globalreg *g, struct globalreg *g0);

/**
 * Allocate nxy registers memory according to global registers supplied
 * @param **r is the destination of nxyreg structure pointer
 * @param *g0 is the pointer to g0 structure 
 * @no no return
 * 
 */ 
void* nxyreg_allocate(struct nxyreg **r, struct globalreg *g0);

/**
 * function return the number of exploder/Gemex card number at specified sfp according
 * to supplied globalreg greg structure, which needs to be properly loaded
 * @param greg globalreg structure
 * @param sfp sfp number
 * @return returns number of exploders/Gemex cards on specified sfp
 */
int get_explo_number(struct globalreg greg, int sfp);

/**
 * function compares the SFP configuration of the 2 global settings
 * if number of nxyters and sfp_in_use are the same functions returns 1
 * @param greg1 globalreg structure
 * @param greg2 globalreg structure
 * @return returns 1 if SFP confuguration of the 2 global registers are the same 
 * 
 */ 
int check_sfp_configuration(struct globalreg greg1, struct globalreg greg2);

/**
 * Prints the global register values and configuration to stdout
 * @param greg globalreg structure to be printed
 */ 
void print_global(struct globalreg greg);

/**
 * Loads the nxyter and global register and configuration from txt file
 * Globalreg structure *greg will be updated according to the file settings.
 * the function will allocate memory for nxyreg structures and returns pointer to it.
 * The user is responsible to free the memory
 * @param filename name of the txt file
 * @param *greg pointer to globalreg structure
 * @return pointer to nxyreg structure, on NULL on failure
 * @warning user should free the returned pointer to nxyreg structures
 */ 
struct nxyreg* load_nxy_file(char *filename, struct globalreg *greg);



/**
 * Prints the nxyreg values to stdout.
 * Only one nxyter is printed, to print more function has to be looped
 * @param reg nxyreg structure to be printed
 */ 
void print_nxy_reg(struct nxyreg reg);



/** 
 * Saves the global settings and nxyter registers to the txt file
 * @param name name of the file to be saved
 * @param greg globalreg structure to be saved
 * @param *r pointer to nxyreg structures to be saved
 * @return 1 on success, 0 on failure
 */
int save_nxy_file(char *name, struct globalreg greg, struct nxyreg *r);

/** 
 * Saves the global settings and nxyter registers to the char* string
 * @param s, already allocated memory
 * @param greg globalreg structure to be saved
 * @param *r pointer to nxyreg structures to be saved
 * @return char* on success, NULL on failure
 */
char* save_nxy_str(char *s, struct globalreg greg, struct nxyreg *r);

/** 
 * Saves the global settings and nxyter registers to the file in binary form.
 * @param name name of the file to be saved
 * @param greg globalreg structure to be saved
 * @param *r pointer to nxyreg structures to be saved
 * @return 1 on success, 0 on failure
 */
int save_bin_file(char *name, struct globalreg greg, struct nxyreg *r);


/** 
 * Loads the global settings and nxyter registers from binary file.
 * memory for nxyreg structures will be allocated,
 * the user is responsible to free it.
 * @param name name of the file to be saved
 * @param greg globalreg structure to be saved
 * @return pointer to the nxyreg structures , NULL on failure
 */
struct nxyreg* load_bin_file(char *name, struct globalreg *greg);
#endif
