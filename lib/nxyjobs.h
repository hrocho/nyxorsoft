/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * here are the functions used to calibrate  nxyters from MBS, or read/manipulate settings
 * this is intented to used inside MBS
 * 
 */ 
 
#ifndef NXYJOBS_H
#define NXYJOBS_H

#include <stdlib.h>
#include "nxyreg.h"
#define MAXSTAT 1000
#define MAX_JOBS 20



struct nxyreg *_r0;
struct globalreg _greg0;
struct nxyreg *_r;
struct globalreg _greg;

char *nxy_socket_name;

// struct to store hits for 1 nxyter
struct nxyhits{
	int num[128];
	int hits[128][MAXSTAT];
};

struct _nxydata{
	struct nxyhits *phits;
	int nxy_number;
};
struct _nxydata nxydata; //structure to hold collected data from all nxyters


struct nxyres2{
	int data[128];
	double val;
};

struct nxyres2 *nxyresult; //structure to keep results from the measurement and scans


typedef enum {	NXYJOB_VBIASS_SCAN=1,
				NXYJOB_VBIASS2_SCAN,
				NXYJOB_VBIAS_SCAN_BOTH,
				NXYJOB_VBIASS_SCAN_AT,
				NXYJOB_VTH_SCAN,
				NXYJOB_LOCALTH_SCAN,
				NXYJOB_LOCALTH_EQ,
				NXYJOB_BASELINES_READ,
				NXYJOB_INTERNAL_CALIBRATION,
				NXYJOB_SAVE_CONFIG} nxyjobs_t;


struct _nxyjobs{
	nxyjobs_t q[MAX_JOBS];
	int par[MAX_JOBS];
	};

int (*nxyjob_function_init)();
int (*nxyjob_function_main)();
int (*nxyjob_function_done)();
int nxyjob_parameter;


int nxydata_event_counter; //count event in saved data
int nxydata_job_stage; //job flow control number
int nxydata_ready;
int nxydata_status;

unsigned int *nxydata_pdata; //pointer to data stream created by MBS 

struct _nxyjobs nxyjobs;

double nxyhits_median(struct nxyhits *n, int ch);
double nxyhits_nhitmean(struct nxyhits *n);


/// calibration functions definitions
int nxyjob_vbiass_init();
int nxyjob_vbiass_main();
int nxyjob_vbiass_done();

int nxyjob_vbiass2_init();
int nxyjob_vbiass2_main();
int nxyjob_vbiass2_done();

int nxyjob_vbiasboth_init();
int nxyjob_vbiasboth_main();
int nxyjob_vbiasboth_done();

int nxyjob_vbiass_at_init();
int nxyjob_vbiass_at_main();
int nxyjob_vbiass_at_done();

int nxyjob_vth_init();
int nxyjob_vth_main();
int nxyjob_vth_done();

int nxyjob_lth_init();
int nxyjob_lth_main();
int nxyjob_lth_done();

int nxyjob_ltheq_init();
int nxyjob_ltheq_main();
int nxyjob_ltheq_done();

int nxyjob_baselines_init();
int nxyjob_baselines_main();
int nxyjob_baselines_done();

int nxyjob_internal_calibration_init();
int nxyjob_internal_calibration_main();
int nxyjob_internal_calibration_done();

int nxyjob_save_config_init();
int nxyjob_save_config_main();
int nxyjob_save_config_done();


int nxyjob_set();
int nxyjob_process(long *);
int nxyjob_add(nxyjobs_t j, int par);
int nxydata_init(char *_socket_name);
int nxydata_clean();
void nxydata_reset();

//#define nxyresult_allocate(p,type,num) if(p!=NULL)free(p);p = (type*)malloc(sizeof(type)*num);

#endif
