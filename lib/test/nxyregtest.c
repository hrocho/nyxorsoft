#include "../nxyreg.c"
#define NXY_FILE "./nxy_nyxor_set.txt"

int main(){

struct nxyreg *r,*r1;
struct globalreg greg,greg1;
int res;

printf("----------- load_nxy_file test ----------\n");
r=load_nxy_file(NXY_FILE,&greg);
if(r==NULL){
    printf("Load failed\n");
    return 0;
}
if(greg.nxy_number!=2){
    printf("Wrong number of nxyters found \n");
    return 0;
}
print_global(greg);
print_nxy_reg(r[0]);
printf("file loaded\n");

printf("----------- Saving nxy file test ---------\n");
res = save_nxy_file("a",greg,r);
if(res != 1){
    printf("Saving failed\n");
    return 0;
}
r1=load_nxy_file("a",&greg1);
if(r1==NULL){
    printf("Load new file failed\n");
    return 0;
}
save_nxy_file("b",greg1,r1);

printf("----------- Saving nxy cfg string ---------\n");
char *s1,*s2;
s1 = (char *)malloc(10000);
s2 = (char *)malloc(10000);
memset(s1,0,10000);
memset(s2,0,10000);

s1 = save_nxy_str(s1,greg,r);
if(s1==NULL){
    printf("Save string failed failed\n");
    return 0;
}

FILE *fw;
fw = fopen("c","w");
fputs(s1,fw);
fclose(fw);
free(s1);
free(s2);


free(r);
free(r1);



printf("done\n");
}
