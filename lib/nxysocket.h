/**
 * @file 
 * A. Prochazka
 * 11.1.2016
 * functions for connecting to nyxor daemon
 */
  
#ifndef NXYSOCKET_H
#define NXYSOCKET_H
#include "nxyreg.h"
#include <stddef.h>
#define NXYD_PORT "3490"

#ifdef NXYDAEMON_BUILD
#define MG_DISABLE_MQTT
#define MG_DISABLE_JSON_RPC
#define MG_DISABLE_MQTT
#define MG_DISABLE_HTTP_DIGEST_AUTH
#define MG_DISABLE_SHA1
#endif
/** filedescriptor for socket to connect to nxyter daemon **/
int nxyd_fd;


/**
 * Structure to hold buffer for reply and allocated length
 * 
 */ 
struct nxy_reply_struct{
    char *msg;
    size_t len;
    size_t size;
} nxy_reply;

#ifdef NXYDAEMON_BUILD
/**
 * structure to store connection parameters
 * 
 */ 
struct nxy_connection_struct{
    int client_socket_fd;
    int protocol_type;
    int reply_type;
    int com;
    struct nxy_reply_struct *nxyreply;
}nxy_connection;

enum nxy_protocol_type {NXYD_PROTOCOL=1, HTTP_PROTOCOL=2};

int nxy_connection_accept(struct nxy_connection_struct *con);
int nxy_connection_init(struct nxy_connection_struct *con,struct nxy_reply_struct *reply);
int nxy_connection_close(struct nxy_connection_struct *con);

int nxy_connection_send(struct nxy_connection_struct *con);
int nxy_connection_error(struct nxy_connection_struct *con);
int nxy_connection_OK(struct nxy_connection_struct *con);
#endif

/**
 * initialize reply message buffer
 * the size will be determined from number if nxyter to avoid resizing
 * @param  nxynum - number of nxyter, to guess the buffer size
 * @return pointer to allocated memory
 */ 
void* nxy_reply_msg_init(int nxynum);

/**
 * clear the buffer, this just zeroes the buffer
 * it does not free the memory
 *  
 */ 
void nxy_reply_msg_clear();
///////////////////  Socket helping function //////////////////////////
/** 
 * Sends the data stream *buf of length *len to the network socked s
 * network socket should be already configured
 * len will be updated by length of actually sent data
 * @param s network socket
 * @param *buf data to send
 * @param *len length of the data to send 
 * @return 1 in success, -1 on failure
 */ 
int sendall(int s, char *buf, int *len);

/**
 * Receive the data stream of length len from network socket s to memory *buf
 * network socket should be already configured
 * @param s network socket
 * @param *buf pointer to destination memory 
 * @param len length of the data to be received
 * @return length of the received data
 */ 
int recvall(int s, char *buf, int len);

/**
 * Packs the data stream *data of length *length into header and trailers with command com
 * the function allocate another buffer which already includes header, data and trailer
 * the length variable will be updated with new size including header+data+trailer
 * the new data stream will be allocated and returned, user is responsible to free it.
 * @param com command number to be packed
 * @param *length length of the data
 * @param *data pointer to data memory 
 * @return pointer to allocated packet data, or NULL on failure
 * 
 */ 
char* pack(int com, int *length, char *data);

/**
 * Unpacks the data from network socket fd,
 * The function will check header and trailer and update *len with length of the data stream received
 * The function will allocate the buffer to store received data.
 * The user is responsible to free it.
 * @param fd network socket
 * @param *len pointer to the integer where length will be stored
 * @param *com pointer to the integet where command number will be stored
 * @return pointer to the received data, or NULL on failure
 */ 
char* unpackdata(int fd,int *len,int *com);

#ifdef NXYDAEMON_BUILD
//// just a test
int unpackdata2(int fd,int *len,int *com);
#endif

/**
 * Creates the data stream from globalreg and nxyreg structures specified in parameters
 * the data stream will be allocated and len variable updated with actuall size.
 * user is responsible to free the memory
 * @param greg globalreg structure to be written
 * @param *r pointer to nxyreg structures to be written
 * @param *len is pointer to length variable
 * @return pointer to the data stream or NULL on failure
 */ 
char* nxydata2str(struct globalreg greg, struct nxyreg *r,int *len);


/**
 * Converts the data stream with binary coded globalreg and nxyreg structures
 * to globalreg and nxyreg structures.
 * memory for nxyreg structures will be allocated. User is responsible to free it.
 * @param buf pointer to data stream to be read
 * @param *greg pointer to globalreg structure, which will be updated
 * @return pointer to nxyreg structures or NULL on failure
 */ 
struct nxyreg* str2nxydata(char* buf, struct globalreg *greg);

/**
 * Get the filedescriptor of network socket for connection to nxyter daemon
 * nxy_fd global variable will be updated by filedescriptor nad will be returned as well.
 * @param sname name of the computer where nxyter daemon is running
 * @return nxy_fd file descriptor for connection to sname mashine or -1 on failure
 */ 
int connect_daemon(char *sname);


/**
 * Close the network connection with the nxyter daemon running on socket nxy_fd
 * @return 1 on success, -1 on failure
 */
int close_daemon();

/**
 * Reads the current nxyter settings from nxydaemon on network socket nxy_fd.
 * nxy_fd needs to be already configured by callin connect_daemon() function.
 * Retreived configuration will be written to *r and greg structures.
 * @param **r pointer to pointer to nxyreg structures \
 * @param *greg pointer to globalreg structure
 * @return 1 on success, -1 on failure
 */
int read_daemon(struct nxyreg **r, struct globalreg *greg);

/**
 * Writes the configurations specified by r and greg to nxyter daemon running on nxy_fd.
 * nxy_fd needs to be already configured by callin connect_daemon() function.
 * The register sets to be updated can be choosen by mode parameter.
 * The mode parameter should correspond to nxy_update enum.
 * @param *r pointer to nxyreg structures \
 * @param greg globalreg structure
 * @param mode update mode, should be from nxy_update_t enum.
 * @return 1 on success, -2 on sending failure, -1 on updating error
 */
int update_daemon(struct nxyreg *r, struct globalreg greg,int mode);


/**
 * Very similar to update_daemon, but resets the nxyters as well.
 * By default all registers will be updated.
 * The testmode parameters can be set to choose working mode.
 * - testmode = 0 is normal mode
 * - testmode = 1 is test mode
 * @param *rr pointer to nxyreg structures \
 * @param gg globalreg structure
 * @param testmode testmode selection
 * @return 1 on success, -2 on sending failure, -1 on updating error
 */
int reset_daemon(struct nxyreg *rr, struct globalreg gg, int testmode);




#endif
